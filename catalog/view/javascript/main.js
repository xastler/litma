$(document).ready(function () {

    $("#search-btn").click(function () {
        $('#search-btn').addClass('open');
        $('.header-catalog').addClass('open');
    });

    $(".footer-info-btn").click(function () {
        $(this).next('ul').slideToggle();
    });

    $(".footer-menu-btn").click(function () {
        $(this).next('ul').slideToggle();
    });

    $(".catalog_category-list i").click(function () {
        if (!$(this).hasClass('active')) {
            $('.catalog_category-list i').removeClass('active');
            $('.catalog_category-list-lvl').removeClass('active');
            $(this).addClass('active');
            $(this).next().addClass('active');
        } else {
            $('.catalog_category-list i').removeClass('active');
            $('.catalog_category-list-lvl').removeClass('active');
        }
    });
    $(".catalog_collection-list i").click(function () {
        if (!$(this).hasClass('active')) {
            $('.catalog_collection-list i').removeClass('active');
            $('.catalog_collection-list-lvl').removeClass('active');
            $(this).addClass('active');
            $(this).next().addClass('active');
        } else {
            $('.catalog_collection-list i').removeClass('active');
            $('.catalog_collection-list-lvl').removeClass('active');
        }
    });

    $(document).on('click', function (e) {
        if (!$(e.target).closest("#search").length && !$(e.target).closest("#search-btn").length) {
            $('#search-btn').removeClass('open');
            $('.header-catalog').removeClass('open');
        }
        e.stopPropagation();
    });
    $('body').on('click', '.ias_trigger a', function (e) {
        e.preventDefault();
        var tar = $(this);
        $('.pagination li').each(function (i, el) {
            if ($(el).hasClass('active')) {
                $(el).removeClass('active');
                $(el).next().addClass('active');
                // $("<div class='load-" + i + "'></div>").appendTo('.bnews-list');
                $("<div class='load'></div>").appendTo('body');
                $('.load').hide();
                // console.log($(el).next().find('a').attr('href'));
                // console.log(encodeURIComponent($(el).next().find('a').attr('href')));
                // return false;
                $('.load').load(encodeURI($(el).next().find('a').attr('href')) + ' .products .row .product-layout ', function () {
                    $("#products .row").append($('.load>.product-layout'));
                    $('.load').detach();
                });
                // $('#mfilter-content-container').parent('div').find('.pagination').append('<div id="ajaxblock" style="width:' + w + 'px;height:30px;margin-top:20px;text-align:center;border:none !important;"><img src="../image/preload.gif" /></div>');
                if (i + 3 == $('.pagination li').length) {
                    tar.hide();
                }
                return false;
            }
        });
    });
    // $('body').on('click', '.ias_trigger a', function (e) {
    //     e.preventDefault();
    //     var tar = $(this);
    //     $('.pagination li').each(function (i, el) {
    //         if ($(el).hasClass('active')) {
    //             $(el).removeClass('active');
    //             $(el).next().addClass('active');
    //             $("<div class='row load-" + i + "'></div>").appendTo('.products .product-layout');
    //             $('.load-' + i).hide();
    //             // console.log($(el).next().find('a').attr('href'));
    //             // console.log(encodeURIComponent($(el).next().find('a').attr('href')));
    //             // return false;
    //             $('.load-' + i).load(encodeURI($(el).next().find('a').attr('href')) + ' .products', function () {
    //                 $('.load-' + i).fadeIn(500);
    //             });
    //             // $('#mfilter-content-container').parent('div').find('.pagination').append('<div id="ajaxblock" style="width:' + w + 'px;height:30px;margin-top:20px;text-align:center;border:none !important;"><img src="../image/preload.gif" /></div>');
    //             if (i + 4 == $('.pagination li').length) {
    //                 tar.hide();
    //             }
    //             return false;
    //         }
    //     });
    // });
    $('.minus').click(function () {
        var $input = $(this).parent().find('input[name="quantity"]');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input[name="quantity"]');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
    $('.checkout-prev').click(function () {
        $('#simplecheckout_button_back').click();
    });
    $('.checkout-order').click(function () {
        $('#button-confirm').click();
        $('#simplecheckout_button_confirm').click();
    });

    $('.header-btn_menu').click(function () {
        $('.left_bar').addClass('open');
    });
    $('.left_bar-close').click(function () {
        $('.left_bar').removeClass('open');
    });

    $('.header-btn_category').click(function () {
        $('.right_bar').addClass('open');
    });
    $('.right_bar-close').click(function () {
        $('.right_bar').removeClass('open');
    });

    $('.mfilter-box h2').click(function () {
        $(this).toggleClass('open');
        $(this).next('div').slideToggle();
    });

    $('.catalog_category h2').click(function () {
        $(this).toggleClass('open');
        $(this).next('ul').slideToggle();
    });
    $('.right_bar-menu i').click(function () {
        $(this).next('ul').slideToggle();
        $(this).toggleClass('active');
    });

    $(function () {
        $('select').selectric({
                disableOnMobile: false,
                nativeOnMobile: false
            }
        );
    });




    $('input[type=tel], input[name="phone"], #customer_telephone, input[name="telephone"]').mask('+38 (999) 999-99-99');
    var offset = $('header').offset();

    if ($(window).scrollTop() > offset.top) {
        $('header').addClass('fixed');
    }
    else {
        $('header').removeClass('fixed');
    }
    $(window).scroll(function () {
        if ($(window).scrollTop() > offset.top) {
            $('header').addClass('fixed');
        }
        else {
            $('header').removeClass('fixed');
        }
    });
    if ($(window).width() < 991) {
        $('.left_bar .container').append($('#language'));
    }
});
    if ($(window).width() < 991) {
        $('.left_bar .container').append($('#language'));
    }
