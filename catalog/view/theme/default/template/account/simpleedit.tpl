<?php if (!$ajax && !$popup && !$as_module) { ?>
<?php $simple_page = 'simpleedit'; include $simple_header; ?>
<div class="simple-content">
<?php } ?>
    <?php if (!$ajax || ($ajax && $popup)) { ?>
    <script  >
    (function($) {
    <?php if (!$popup && !$ajax) { ?>
        $(function(){
    <?php } ?>
            if (typeof Simplepage === "function") {
                var simplepage = new Simplepage({
                    additionalParams: "<?php echo $additional_params ?>",
                    additionalPath: "<?php echo $additional_path ?>",
                    mainUrl: "<?php echo $action; ?>",
                    mainContainer: "#simplepage_form",
                    scrollToError: <?php echo $scroll_to_error ? 1 : 0 ?>,
                    javascriptCallback: function() {<?php echo $javascript_callback ?>}
                });

                simplepage.init();
            }
    <?php if (!$popup && !$ajax) { ?>
        });
    <?php } ?>
    })(jQuery || $);
    </script>
    <?php } ?>
    <div class="row">
        <div class="col-md-3">
            <ul class="account_left_bar">
                <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil_profil; ?></a></li>
                <li><a href="<?php echo $link_password; ?>"><?php echo $text_profil_password; ?></a></li>
                <li><a href="<?php echo $link_history; ?>"><?php echo $text_profil_history; ?></a></li>
                <li><a href="<?php echo $link_logout; ?>"><?php echo $text_profil_logout; ?></a></li>
            </ul>
        </div>
        <div class="col-md-6">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="simplepage_form">
                <div class="simpleregister" id="simpleedit">
                    <?php if ($error_warning) { ?>
                        <div class="warning alert alert-danger"><?php echo $error_warning; ?></div>
                    <?php } ?>
                        <?php foreach ($rows as $row) { ?>
                            <?php echo $row ?>
                        <?php } ?>
                    <div class="text-center">
                        <a class="button btn-default button_oc btn" data-onclick="submit" id="simpleregister_button_confirm"><span><?php echo $button_continue; ?></span></a>
                    </div>
                </div>
                <?php if ($redirect) { ?>
                    <script  >
                        location = "<?php echo $redirect ?>";
                    </script>
                <?php } ?>
            </form>
        </div>
    </div>

<?php if (!$ajax && !$popup && !$as_module) { ?>
</div>
<?php include $simple_footer ?>
<?php } ?>