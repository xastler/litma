<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?php if ($success) { ?>
                            <br>
                            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
                        <?php } ?>
                        <?php if ($error_warning) { ?>
                            <br>
                            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                        <?php } ?>
                        <br>
                        <div class="well">
                            <div class="text-center">
                                <h1><?php echo $text_i_am_returning_customer; ?></h1>
                            </div>
                            <br>
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <input type="text" name="email" value="<?php echo $email; ?>"
                                           placeholder="<?php echo $entry_email; ?>" id="input-email"
                                           class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" value="<?php echo $password; ?>"
                                           placeholder="<?php echo $entry_password; ?>" id="input-password"
                                           class="form-control"/>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="pull-right">
                                            <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                                        </div>
                                    <div class="pull-left">
                                        <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a>
                                    </div>
                                    </div>

                                </div>
                                <div class="text-center">
                                    <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-default"/>
                                </div>
                                <?php if ($redirect) { ?>
                                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                </div>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
<?php echo $footer; ?>