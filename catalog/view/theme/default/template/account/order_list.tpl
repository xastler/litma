<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="text-center">
      <h1><?php echo $heading_title; ?></h1>
        </div>
        <div class="row">
            <div class="col-md-3">
                <ul class="account_left_bar">
                    <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil_profil; ?></a></li>
                    <li><a href="<?php echo $link_password; ?>"><?php echo $text_profil_password; ?></a></li>
                    <li><a href="<?php echo $link_history; ?>"><?php echo $text_profil_history; ?></a></li>
                    <li><a href="<?php echo $link_logout; ?>"><?php echo $text_profil_logout; ?></a></li>
                </ul>
            </div>
            <div class="col-md-9 col-xs-12">
                <?php if ($orders) { ?>
                    <div class="order_histor_list">
                        <?php foreach ($orders as $order) { ?>
                            <div class="order_histor_item">
                                <div class="history_block">
                                    <div class="options">
                                        <div class="option_item"><?php echo $text_qty; ?> <?php echo $order['qt']; ?></div>
                                    </div>
                                    <div class="price"><?php echo $order['total']; ?></div>
                                </div>
                                <div class="history_block">
                                    <div class="text_status"><?php echo $column_status; ?></div>
                                    <div class="status"><?php echo $order['status']; ?></div>
                                    <div class="date"><?php echo $order['date_added']; ?></div>
                                </div>
                                <div class="history_block">
                                    <div class="inside">
                                        <a href="<?php echo $order['href']; ?>"><?php echo $button_view; ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="text-center"><?php echo $pagination; ?></div>
                <?php }else{ ?>
                    <p><?php echo $text_empty; ?></p>
                <?php } ?>
            </div>
        </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>