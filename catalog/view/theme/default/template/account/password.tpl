<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>

<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="text-center">
            <h1><?php echo $heading_title; ?></h1>
        </div>
        <div class="row">
            <div class="col-md-3">
                <ul class="account_left_bar">
                    <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil_profil; ?></a></li>
                    <li><a href="<?php echo $link_password; ?>"><?php echo $text_profil_password; ?></a></li>
                    <li><a href="<?php echo $link_history; ?>"><?php echo $text_profil_history; ?></a></li>
                    <li><a href="<?php echo $link_logout; ?>"><?php echo $text_profil_logout; ?></a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <fieldset>
                        <div class="form-group required">
                            <div class="col-sm-12">
                                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                                <?php if ($error_password) { ?>
                                    <div class="text-danger"><?php echo $error_password; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group required">
                            <div class="col-sm-12">
                                <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
                                <?php if ($error_confirm) { ?>
                                    <div class="text-danger"><?php echo $error_confirm; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </fieldset>
                    <div class="buttons clearfix">
                        <div class="text-center">
                            <input type="submit" value="<?php echo $text_btn_save; ?>" class="btn btn-default" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>