<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <div id="content" class="col-sm-12"><?php echo $content_top; ?>
        <div class="text-center">
            <h1><?php echo $heading_title; ?></h1>
        </div>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <div class="row">
            <div class="col-md-3">
                <ul class="account_left_bar">
                    <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil_profil; ?></a></li>
                    <li><a href="<?php echo $link_password; ?>"><?php echo $text_profil_password; ?></a></li>
                    <li><a href="<?php echo $link_history; ?>"><?php echo $text_profil_history; ?></a></li>
                    <li><a href="<?php echo $link_logout; ?>"><?php echo $text_profil_logout; ?></a></li>
                </ul>
            </div>
        </div>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
