<?php echo $header; ?>
    <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <?php if($i+1<count($breadcrumbs)) { ?>
                    <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <div class="mega_banner_testimonial">

        <h1><?php echo $heading_title; ?></h1>
    </div>
    <div class="container">

        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <?php if ($products) { ?>
                    <div class="row">
                        <?php foreach ($products as $product) { ?>
                            <div class="product-layout col-lg-3 col-ms-3 col-sm-4 col-xs-6">
                                <div class="product-thumb transition">
                                    <div class="image">
                                        <a href="<?php echo $product['href']; ?>">
                                            <img
                                                    src="<?php echo $product['thumb']; ?>"
                                                    alt="<?php echo $product['name']; ?>"
                                                    title="<?php echo $product['name']; ?>" class="img-responsive"/>
                                            <?php if ($product['thumbs']) { ?>
                                                <img src="<?php echo $product['thumbs']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive image_2" />
                                            <?php } ?>
                                        </a>
                                        <a class="btn-product_wishlist" href="<?php echo $product['remove']; ?>"><i
                                                    class="icon-path"></i></a>
                                        <button class="btn-product_cart" type="button"
                                                onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
                                            <i class="icon-shopping-bag"></i> <span><?php echo $button_cart; ?></span>
                                        </button>
                                    </div>
                                    <div class="caption">
                                        <h4>
                                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        </h4>
                                        <?php if ($product['price']) { ?>
                                            <p class="price">
                                                <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                <?php } else { ?>
                                                    <span class="price-new"><?php echo $product['special']; ?></span>
                                                    <span class="price-old"><?php echo $product['price']; ?></span>
                                                <?php } ?>
                                            </p>
                                        <?php } ?>
                                    </div>


                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <p><?php echo $text_empty; ?></p>
                <?php } ?>


                <div class="buttons clearfix">
                    <div class="pull-right"><a href="/"
                                               class="btn btn-default"><?php echo $button_continue; ?></a></div>
                </div>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
<?php echo $footer; ?>