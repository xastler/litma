<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="mega_banner_testimonial">
    <h1><?php echo $heading_title; ?></h1>
</div>
<div class="container">

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <?php if ($products) { ?>
      <div class="block-sorting">
          <div class="block-sorting-content">
              <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>

              <select id="input-sort" onchange="location = this.value;">
                  <?php foreach ($sorts as $sorts) { ?>
                      <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                          <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                      <?php } else { ?>
                          <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                      <?php } ?>
                  <?php } ?>
              </select>
          </div>
          <div class="block-sorting-content hidden-xs">
              <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>

              <select id="input-limit" onchange="location = this.value;">
                  <?php foreach ($limits as $limits) { ?>
                      <?php if ($limits['value'] == $limit) { ?>
                          <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                      <?php } else { ?>
                          <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                      <?php } ?>
                  <?php } ?>
              </select>
          </div>
  </div>
      <div class="row">
        <?php foreach ($products as $product) { ?>
            <div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-6">
                <div class="product-thumb transition">
                    <div class="image">
                        <a href="<?php echo $product['href']; ?>">
                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                            <?php if ($product['thumbs']) { ?>
                                <img src="<?php echo $product['thumbs']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive image_2" />
                            <?php } ?>
                        </a>
                        <button class="btn-product_wishlist" type="button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-heart"></i><i class="icon-heart-hover"></i></button>
                        <?php if ($product['price']) { ?>
                        <button class="btn-product_cart" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="icon-shopping-bag"></i> <span><?php echo $button_cart; ?></span></button>
                        <?php } ?>
                    </div>
                    <div class="caption">
                        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                        <?php if ($product['price']) { ?>
                            <p class="price">
                                <?php if (!$product['special']) { ?>
                                    <?php echo $product['price']; ?>
                                <?php } else { ?>
                                    <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                <?php } ?>
                            </p>
                        <?php } ?>
                    </div>


                </div>
            </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
<!--        <div class="col-sm-6 text-right">--><?php //echo $results; ?><!--</div>-->
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
