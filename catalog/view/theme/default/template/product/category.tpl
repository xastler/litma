<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>

<div class="mega_banner_top">
    <img src="<?php echo $thumb_max; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"/>
    <h1><?php echo $heading_title; ?></h1>
</div>
<div class="container">

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-md-9 col-sm-8'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <?php /*if ($categories) { ?>
                <h3><?php echo $text_refine; ?></h3>
                <?php if (count($categories) <= 5) { ?>
                    <div class="row">
                        <div class="col-sm-3">
                            <ul>
                                <?php foreach ($categories as $category) { ?>
                                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
                            <div class="col-sm-3">
                                <ul>
                                    <?php foreach ($categories as $category) { ?>
                                        <li>
                                            <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } */?>
            <?php if ($products) { ?>
                <div class="block-sorting">
                    <div class="block-sorting-content sort_category">
                        <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>

                        <select id="input-sort" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                    <option value="<?php echo $sorts['href']; ?>"
                                            selected="selected"><?php echo $sorts['text']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="block-sorting-content hidden-sm hidden-xs">
                        <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>

                        <select id="input-limit" onchange="location = this.value;">
                            <?php foreach ($limits as $limits) { ?>
                                <?php if ($limits['value'] == $limit) { ?>
                                    <option value="<?php echo $limits['href']; ?>"
                                            selected="selected"><?php echo $limits['text']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
<div class="products" id="products">
                <div class="row ">
                    <?php foreach ($products as $product) { ?>
                        <?php $this->partial('product_item', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare)); ?>
                    <?php } ?>
                </div>
</div>
                <?php if ($pagination) { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ias_trigger">
                                <a href="/"><?php echo $text_ajax_pp; ?></a>
                            </div>
                        </div>
                        <div class="col-sm-12"><?php echo $pagination; ?></div>
                    </div>
                <?php } ?>
            <?php } ?>
            <?php if (!$categories && !$products) { ?>
                <p><?php echo $text_empty; ?></p>
                <div class="buttons">
                    <div class="pull-right"><a href="<?php echo $continue; ?>"
                                               class="btn btn-default"><?php echo $button_continue; ?></a></div>
                </div>
            <?php } ?>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
