<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
    <div class="container">

        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <h2><?php echo $heading_title; ?></h2>

                <div class="row">
                    <div class="col-sm-12">
                        <input type="text" name="search" value="<?php echo $search; ?>"
                               placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control"/>
                        <button id="button-search" class="button-search icon-magnifying-glass"/>
                    </div>
                </div>


                <?php if ($products) { ?>
                    <div class="block-sorting">
                        <div class="block-sorting-content">
                            <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>

                            <select id="input-sort" onchange="location = this.value;">
                                <?php foreach ($sorts as $sorts) { ?>
                                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                        <option value="<?php echo $sorts['href']; ?>"
                                                selected="selected"><?php echo $sorts['text']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="block-sorting-content hidden-xs">
                            <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>

                            <select id="input-limit" onchange="location = this.value;">
                                <?php foreach ($limits as $limits) { ?>
                                    <?php if ($limits['value'] == $limit) { ?>
                                        <option value="<?php echo $limits['href']; ?>"
                                                selected="selected"><?php echo $limits['text']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row margin_bottom_60">
                        <?php foreach ($products as $product) { ?>
                            <div class="product-layout col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                <div class="product-thumb transition">
                                    <div class="image">
                                        <a href="<?php echo $product['href']; ?>"><img
                                                    src="<?php echo $product['thumb']; ?>"
                                                    alt="<?php echo $product['name']; ?>"
                                                    title="<?php echo $product['name']; ?>" class="img-responsive"/></a>
                                        <?php if ($product['price']) { ?>

                                            <button class="btn-product_wishlist" type="button"
                                                onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i
                                                    class="icon-heart"></i><i class="icon-heart-hover"></i></button>
                                        <button class="btn-product_cart" type="button"
                                                onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
                                            <i class="icon-shopping-bag"></i> <span><?php echo $button_cart; ?></span>
                                        </button>
                                        <?php } ?>
                                    </div>
                                    <div class="caption">
                                        <h4>
                                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        </h4>
                                        <?php if ($product['price']) { ?>
                                            <p class="price">
                                                <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                <?php } else { ?>
                                                    <span class="price-new"><?php echo $product['special']; ?></span>
                                                    <span class="price-old"><?php echo $product['price']; ?></span>
                                                <?php } ?>
                                            </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                        <!--        <div class="col-sm-6 text-right">--><?php //echo $results; ?><!--</div>-->
                    </div>
                <?php } else { ?>
                    <p><?php echo $text_empty; ?></p>
                <?php } ?>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
    <script><!--
        $('#button-search').bind('click', function () {
            url = 'index.php?route=product/search';

            var search = $('#content input[name=\'search\']').prop('value');

            if (search) {
                url += '&search=' + encodeURIComponent(search);
            }

            var category_id = $('#content select[name=\'category_id\']').prop('value');

            if (category_id > 0) {
                url += '&category_id=' + encodeURIComponent(category_id);
            }

            var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

            if (sub_category) {
                url += '&sub_category=true';
            }

            var filter_description = $('#content input[name=\'description\']:checked').prop('value');

            if (filter_description) {
                url += '&description=true';
            }

            location = url;
        });

        $('#content input[name=\'search\']').bind('keydown', function (e) {
            if (e.keyCode == 13) {
                $('#button-search').trigger('click');
            }
        });

        $('select[name=\'category_id\']').on('change', function () {
            if (this.value == '0') {
                $('input[name=\'sub_category\']').prop('disabled', true);
            } else {
                $('input[name=\'sub_category\']').prop('disabled', false);
            }
        });

        $('select[name=\'category_id\']').trigger('change');
        --></script>
<?php echo $footer; ?>
