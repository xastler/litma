<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
<div class="container">

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> page_product" ><?php echo $content_top; ?>
            <div class="row" itemscope itemtype="http://schema.org/Product">
                <div class="col-sm-6">
                    <div class="product-img">
                        <?php if ($thumb || $images) { ?>
                            <?php if ($thumb) { ?>
                                <div class="product-img-main">
                                    <a class="" href="<?php echo $popup; ?>"
                                       title="<?php echo $heading_title; ?>">
                                        <img itemprop="image"
                                                                                  src="<?php echo $thumb; ?>"
                                             title="<?php echo $heading_title; ?>, <?php echo $text_art; ?>: <?php echo $sku; ?>"
                                             alt="<?php echo $heading_title; ?> (<?php echo $sku; ?>) <?php echo $text_photo; ?> 1"/></a>

                                    <?php if ($special) { ?>
                                        <div class="special">Sale</div>
                                    <?php } ?>

                                    <?php if ($new == 1) { ?>
                                        <div class="new_tov">New</div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <ul class="product-img-list">

                                <?php if ($images) { ?>
                                    <?php $k=2; foreach ($images as $image) { ?>
                                        <li><a href="<?php echo $image['popup']; ?>"
                                               title="<?php echo $heading_title; ?>"> <img
                                                        src="<?php echo $image['thumb']; ?>"
                                                        title="<?php echo $heading_title; ?>, <?php echo $text_art; ?>: <?php echo $sku; ?>"
                                                        alt="<?php echo $heading_title; ?> (<?php echo $sku; ?>) <?php echo $text_photo; ?> <?php echo $k; ?>"/></a></li>
                                    <?php $k++;} ?>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-sm-6" id="product">
                    <div class="product-sku"><?php echo $sku; ?></div>
                    <h1 itemprop="name"><?php echo $heading_title; ?></h1>
                    <div class="product-info">
                        <?php if ($review_status) { ?>
                            <div class="product-info-rating" itemprop="aggregateRating" itemscope
                                 itemtype="http://schema.org/AggregateRating">
                                <p>
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <?php if ($rating < $i) { ?>
                                            <i class="icon-star"></i>
                                        <?php } else { ?>
                                            <i class="icon-star-hover"></i>
                                        <?php } ?>
                                    <?php } ?>
                                </p>
                                <span class="product-info-rating-text">Рейтинг <span
                                            itemprop="ratingValue"><?php echo $rating ?></span>/5 основан на <span
                                            itemprop="reviewCount"><?php echo $reviews_count ?></span> отзывов</span>
                                <a onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a>
                            </div>
                        <?php } ?>
                        <div class="product-info-price">
                            <?php if ($price) { ?>
                                <?php if (!$special) { ?>
                                    <h2><?php echo $price; ?></h2>
                                <?php } else { ?>
                                    <span><?php echo $price; ?></span>
                                    <h2><?php echo $special; ?></h2>
                                <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="product-description" itemprop="description">
                        <?php echo $description; ?>
                    </div>
                    
                    <div class="product-model">
                        <?php if($products_group){ ?>
                        <?php foreach($products_group as $product_group){ ?>
                            <a href="<?=$product_group['href']?>">
                                <img src="<?=$product_group['image']?>" title="<?=$product_group['title']?>" />
                            </a>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    
                    <div class="product-navigation">
                        <div class="product-dimensions">
                            <?php if ($options) { ?>
                                <?php foreach ($options as $option) { ?>
                                    <?php if ($option['type'] == 'select') { ?>
                                        <select name="option[<?php echo $option['product_option_id']; ?>]"
                                                id="input-option<?php echo $option['product_option_id']; ?>">
                                            <option value=""><?php echo $option['name']; ?></option>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    <?php } ?>

                                    <?php /*
                                    <?php if ($option['type'] == 'radio') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio"
                                                                   name="option[<?php echo $option['product_option_id']; ?>]"
                                                                   value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'checkbox') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"
                                                                   name="option[<?php echo $option['product_option_id']; ?>][]"
                                                                   value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'image') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio"
                                                                   name="option[<?php echo $option['product_option_id']; ?>]"
                                                                   value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                            <img src="<?php echo $option_value['image']; ?>"
                                                                 alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                                 class="img-thumbnail"/> <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'text') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]"
                                                   value="<?php echo $option['value']; ?>"
                                                   placeholder="<?php echo $option['name']; ?>"
                                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                                   class="form-control"/>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'textarea') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5"
                                                      placeholder="<?php echo $option['name']; ?>"
                                                      id="input-option<?php echo $option['product_option_id']; ?>"
                                                      class="form-control"><?php echo $option['value']; ?></textarea>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'file') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <button type="button"
                                                    id="button-upload<?php echo $option['product_option_id']; ?>"
                                                    data-loading-text="<?php echo $text_loading; ?>"
                                                    class="btn btn-default btn-block"><i
                                                        class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]"
                                                   value="" id="input-option<?php echo $option['product_option_id']; ?>"/>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'date') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group date">
                                                <input type="text"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD"
                                                       id="input-option<?php echo $option['product_option_id']; ?>"
                                                       class="form-control"/>
                                                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'datetime') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group datetime">
                                                <input type="text"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value="<?php echo $option['value']; ?>"
                                                       data-date-format="YYYY-MM-DD HH:mm"
                                                       id="input-option<?php echo $option['product_option_id']; ?>"
                                                       class="form-control"/>
                                                <span class="input-group-btn">
                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                    </span></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'time') { ?>
                                        <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"
                                                   for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group time">
                                                <input type="text"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value="<?php echo $option['value']; ?>" data-date-format="HH:mm"
                                                       id="input-option<?php echo $option['product_option_id']; ?>"
                                                       class="form-control"/>
                                                <span class="input-group-btn">
                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                    </span></div>
                                        </div>
                                    <?php } ?>
                                    */ ?>
                                <?php } ?>
                            <?php } ?>
                            <?php if($image_table_size){ ?>
                            <a href="<?=$image_table_size?>" class="product-navigation-btn image_table_size"><?php echo $text_btn; ?></a>
                            <?php } ?>
                        </div>
                        <?php if ($price) { ?>
                        <div class="product-navigation-recurrings">
                            <span class="minus"><i class="icon-down-arrow"></i></span>
                            <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2"
                                   id="input-quantity"/>
                            <span class="plus"><i class="icon-down-arrow"></i></span>

                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                        </div>
                        <?php } ?>

                        <?php if ($minimum > 1) { ?>
                            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="product-navigation-buttons">
                        <?php if ($price) { ?>
                        <button type="button" class="btn_wishlist"
                                onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="icon-heart"></i><span><?php echo $text_wishlist; ?></span>
                        </button>

                        <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>"
                                class="btn_cart"><i class="icon-shopping-bag"></i><span><?php echo $button_cart; ?></span></button>
                        <?php } ?>
                    </div>
                    <?php if ($recurrings) { ?>
                        <div class="form-control required">
                            <select name="recurring_id" class="form-control">
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($recurrings as $recurring) { ?>
                                    <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                <?php } ?>
                            </select>
                            <div class="help-block" id="recurring-description"></div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="product-reviews">
                <?php if ($review_status) { ?>
                    <h2><?php echo $tab_review; ?></h2>
                    <div class="product-reviews-list" id="review"></div>
                    <div class="product-reviews-form">
                        <form id="form-review">
                            <h2><?php echo $text_write; ?></h2>
                            <?php if ($review_guest) { ?>
                                <div class="form-inputs">
                                    <div class="form-input required">
                                        <input type="text" name="name" value="" id="input-name" placeholder="<?php echo $entry_name; ?>"/>
                                    </div>
                                    <div class="form-input required">
                                        <input type="text" name="email" value="" id="input-email" placeholder="<?php echo $entry_email; ?>"/>
                                    </div>
                                    <div class="form-rating required">
                                        <p><?php echo $entry_rating; ?></p>
                                        <div class="form-rating-star">
                                            <input id="star-5" type="radio" name="rating" value="5">
                                            <label for="star-5" class="icon-star-grey"></label>

                                            <input id="star-4" type="radio" name="rating" value="4">
                                            <label for="star-4" class="icon-star-grey"></label>

                                            <input id="star-3" type="radio" name="rating" value="3">
                                            <label for="star-3" class="icon-star-grey"></label>

                                            <input id="star-2" type="radio" name="rating" value="2">
                                            <label for="star-2" class="icon-star-grey"></label>

                                            <input id="star-1" type="radio" name="rating" value="1">
                                            <label for="star-1" class="icon-star-grey"></label>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-textarea">
                                    <textarea name="text" rows="5" id="input-review" placeholder="<?php echo $entry_review; ?>"></textarea>
                                </div>
                                <div class="buttons clearfix">
                                        <button type="button" id="button-review"
                                                data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_ok; ?></button>
                                </div>
                            <?php } else { ?>
                                <?php echo $text_login; ?>
                            <?php } ?>
                        </form>
                    </div>
                <?php } ?>
            </div>
            <?php if ($products) { ?>
                <h3><?php echo $text_related; ?></h3>
                <div class="row">
                    <?php $i = 0; ?>
                    <?php foreach ($products as $product) { ?>
                        <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare)); ?>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($tags) { ?>
                <p><?php echo $text_tags; ?>
                    <?php for ($i = 0; $i < count($tags); $i++) { ?>
                        <?php if ($i < (count($tags) - 1)) { ?>
                            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
                        <?php } else { ?>
                            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                        <?php } ?>
                    <?php } ?>
                </p>
            <?php } ?>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<script><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#recurring-description').html('');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script><!--
    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'],  #product select'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {

                    $('.header').before('<div class="popups_block"><i class="fa fa-check-circle"></i> ' + json['success']);

                    setTimeout(function () {
                        $('.popups_block').detach();
                    }, 3100);

                    $('#cart').html('<i class="icon-shopping-bag"></i><span>' + json['total'] + '</span>');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script><!--
    $('#review').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                $('.popups_block, .alert').remove();

                if (json['error']) {
                    $('#form-review textarea').after('<div class="alert "><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('.header').before('<div class="popups_block"><i class="fa fa-check-circle"></i> ' + json['success']);

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    $(document).ready(function () {
        $('.product-img , .thumbnails').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery: {
                enabled: true
            }
        });

        $('.image_table_size').magnificPopup({
          type: 'image'
          // other options
        });
    });
    //--></script>
<?php echo $footer; ?>
