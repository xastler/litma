<?php if ($reviews) { ?>
    <ul>
        <?php foreach ($reviews as $review) { ?>
            <li>
                <div class="reviews-top">
                    <div class="reviews-name">
                        <?php echo $review['author']; ?>
                        <span><?php echo $review['date_added']; ?>
                    </div>
                    <div class="reviews-rating">
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php if ($review['rating'] < $i) { ?>
                            <i class="icon-star"></i>
                        <?php } else { ?>
                            <i class="icon-star-hover"></i>
                        <?php } ?>
                        <?php } ?></td>
                    </div>
                </div>
                <div class="reviews-text">
                    <?php echo $review['text']; ?>
                </div>
            </li>
        <?php } ?>
    </ul>
<?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
<?php } ?>
