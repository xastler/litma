<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    <div class="mega_banner_top">
        <img src="<?php echo $thumb_max; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"/>
        <h1><?php echo $heading_title; ?></h1>
    </div>
        <div class="banner_category collection">
            <ul>
                <?php $key=0; foreach ($categories as $category) {  ?>
                    <li>
                        <div class="container">
                            <div class="banner_category_collection">
                                <div class="banner_category-img">
                                    <img src="<?php echo $category['image']; ?>" alt="">
                                </div>
                                <div class="banner_category-content">
                                    <div class="banner_category-name"><?php echo $text_collection; echo $category['name']; ?></div>
                                    <p><?php echo $category['description']; ?></p>
                                </div>
                            </div>
                            <div class="collection-slider">
                                <h2><?php echo $text_sliders; ?></h2>
                                <div class="module_latest-slider">
                                    <div class="module_latest-prev key<?php echo $key; ?>">
                                        <i class="icon-right-arrow"></i>
                                    </div>
                                    <div class="module_latest-slide key<?php echo $key; ?>">
                                        <?php foreach ($category['products'] as $product) { ?>
                                            <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
                                        <?php } ?>
                                    </div>
                                    <div class="module_latest-next key<?php echo $key; ?>">
                                        <i class="icon-right-arrow"></i>
                                    </div>
                                </div>
                            </div>
                            <a class="btn-collection" href="<?php echo $category['href']; ?>"><?php echo $text_open; ?><i class="icon-right-arrow"></i></a>
                            <script>
                                $('.module_latest-slide.key<?php echo $key; ?>').slick({
                                    dots: true,
                                    infinite: true,
                                    speed: 300,
                                    prevArrow: $('.module_latest-prev.key<?php echo $key; ?>'),
                                    nextArrow: $('.module_latest-next.key<?php echo $key; ?>'),
                                    slidesToShow: 4,
                                    slidesToScroll: 4,
                                    responsive: [
                                        {
                                            breakpoint: 1300,
                                            settings: {
                                                slidesToShow: 3,
                                                slidesToScroll: 3,
                                                infinite: true,
                                                dots: true
                                            }
                                        },
                                        {
                                            breakpoint: 992,
                                            settings: {
                                                slidesToShow: 3,
                                                slidesToScroll: 3,
                                                infinite: true,
                                                dots: true,
                                                arrows: false
                                            }
                                        },
                                        {
                                            breakpoint: 768,
                                            settings: {
                                                slidesToShow: 2,
                                                slidesToScroll: 2,
                                                infinite: true,
                                                dots: true,
                                                arrows: false
                                            }
                                        },
                                        {
                                            breakpoint: 551,
                                            settings: {
                                                slidesToShow: 1,
                                                slidesToScroll: 1,
                                                infinite: true,
                                                dots: true,
                                                arrows: false
                                            }
                                        }
                                    ]
                                });
                            </script>
                        </div>
                    </li>
                <?php $key++; } ?>
            </ul>
        </div>
<?php echo $footer; ?>


