<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="mega_banner_top">
    <img src="<?php echo $thumb_max; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"/>
    <h1><?php echo $heading_title; ?></h1>
</div>
<div class="container">
    <div class="banner_category">
        <ul>
            <?php foreach ($categories as $category) {  ?>
                <li>
                    <div class="banner_category-img">
                        <img src="<?php echo $category['image']; ?>" alt="">
                    </div>
                    <div class="banner_category-content">
                        <div class="banner_category-name"><?php echo $category['name']; ?></div>
                        <p><?php echo $category['description']; ?></p>
                        <a href="<?php echo $category['href']; ?>"><?php echo $text_open; ?><i class="icon-right-arrow"></i></a>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="bslider_category">
    <?php foreach ($categories as $category) {  ?>
        <div class="bslider_category-block">
            <div class="bslider_category-img">
                <div class="bslider_category-imgll">
                    <img src="<?php echo $category['image']; ?>" alt="">
                </div>
            </div>
            <div class="bslider_category-content">
                <div class="bslider_category-name"><?php echo $category['name']; ?></div>
                <p><?php echo $category['description']; ?></p>
                <a href="<?php echo $category['href']; ?>"><?php echo $text_open; ?><i class="icon-right-arrow"></i></a>
            </div>
        </div>
    <?php } ?>
</div>
<script>
    $('.bslider_category').slick({
        slidesToShow:1,
        slidesToScroll: 1,
        infinite: true,
        dots:true,
        arrows:false
    });
    </script>
<?php echo $footer; ?>



