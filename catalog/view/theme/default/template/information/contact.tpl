<?php echo $header; ?>
<ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <?php if($i+1<count($breadcrumbs)) { ?>
                <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                    <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                </a>
            <?php } else { ?>
                <span itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>">
                    <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                </span>
            <?php } ?>
            <meta itemprop="position" content="<?php echo $i?>" />
        </li>
    <?php } ?>
</ul>
<div class="mega_banner_top">
    <img src="<?php echo $image; ?>" alt="<?php echo $store; ?>" title="<?php echo $store; ?>"/>
    <h1><?php echo $heading_title; ?></h1>
</div>

<div class="container">

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="contact_info">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="contact_info-block">
                            <i class="icon-contact-placeholder"></i>
                            <p><?php echo $text_address; ?></p>
                            <div class="contact_info-text">
                                <?php echo $cf_address; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="contact_info-block">
                            <i class="icon-contact-email"></i>
                            <p><?php echo $text_sales; ?></p>
                            <div class="contact_info-text">
                                <a href="mail:<?php echo $cf_email; ?>"><?php echo $cf_email; ?></a>
                                <a href="mail:<?php echo $cf_email2; ?>"><?php echo $cf_email2; ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="contact_info-block">
                            <i class="icon-contact-call"></i>
                            <p><?php echo $text_telephone; ?></p>
                            <div class="contact_info-text">
                                <ul>
                                    <?php if ($telephone1) { ?>
                                        <li>
                                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>">
                                                <?php echo $telephone1; ?>
                                            </a>
                                        </li>
                                    <?php }?>
                                    <?php if ($telephone2) { ?>
                                        <li>
                                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">
                                                <?php echo $telephone2; ?>
                                            </a>
                                        </li>
                                    <?php }?>
                                    <?php if ($telephone3) { ?>
                                        <li>
                                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>">
                                                <?php echo $telephone3; ?>
                                            </a>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact_form">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <fieldset>
                        <legend><?php echo $text_contact; ?></legend>
                        <div class="contact_form-inputs">
                            <div class="contact_form-input required">
                                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" placeholder="<?php echo $entry_name; ?>"/>
                                <?php if ($error_name) { ?>
                                    <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>
                            </div>
                            <div class="contact_form-input required">
                                <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" placeholder="<?php echo $entry_email; ?>"/>
                                <?php if ($error_email) { ?>
                                    <div class="text-danger"><?php echo $error_email; ?></div>
                                <?php } ?>
                            </div>
                            <div class="contact_form-input required">
                                <input type="text" name="phone" value="<?php echo $phone; ?>" id="input-phone" placeholder="<?php echo $entry_phone; ?>"/>
                            </div>
                        </div>
                        <div class="contact_form-textarea required">
                                <textarea name="enquiry" rows="10" id="input-enquiry" placeholder="<?php echo $entry_enquiry; ?>"><?php echo $enquiry; ?></textarea>
                                <?php if ($error_enquiry) { ?>
                                    <div class="text-danger"><?php echo $error_enquiry; ?></div>
                                <?php } ?>
                        </div>
                        <?php echo $captcha; ?>
                    </fieldset>
                    <div class="buttons">
                        <input type="submit" value="<?php echo $button_submit; ?>"/>
                    </div>
                </form>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<address style="display: none">
    <div itemscope itemtype="http://schema.org/PostalAddress">
        <span itemprop="name"><?php echo $store ?></span><br/>
        <?php echo $cf_address; ?>
        Телефон: <span itemprop="telephone"><?php echo $telephone1 ?></span><br/>
        E-mail: <span itemprop="email"><?php echo $cf_email ?></span>
    </div>
    <?php //echo $address; ?>
</address>
<?php echo $footer; ?>
