<?php echo $header; ?>
<?php /*<ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <?php if($i+1<count($breadcrumbs)) { ?>
                <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                    <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
            <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
            <?php } ?>
        </li>
    <?php } ?>
</ul>*/?>
<ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <?php if($i+1<count($breadcrumbs)) { ?>
                <a itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>" href="<?php echo $breadcrumb['href']; ?>">
                    <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                </a>
            <?php } else { ?>
                <span itemscope itemtype="http://schema.org/Thing" itemprop="item" id="<?php echo $breadcrumb['href']; ?>">
                    <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                </span>
            <?php } ?>
            <meta itemprop="position" content="<?php echo $i?>" />
        </li>
    <?php } ?>
</ul>


    <?php echo $content_top; ?>
    <?php echo $description; ?>
    <?php echo $content_bottom; ?>>
<?php echo $footer; ?>