<div class="mega_banner_testimonial">
    <h1><?php echo $main_title; ?></h1>
</div>
<div class="container">
<?php if ($is_author) { ?>
    <?php if ($author_image || $author_desc) { ?>
        <div class="category-info">
            <?php if ($author_image) { ?>
                <div class="image"><img src="<?php echo $author_image; ?>" alt="<?php echo $author; ?>"/></div>
            <?php } ?>
            <?php if ($author_desc) { ?>
                <?php echo $author_desc; ?>
            <?php } ?>
        </div>
    <?php } ?>
<?php } ?>
<?php if ($is_category) { ?>
    <?php if ($thumb || $description) { ?>
        <div class="category-info">
            <?php if ($thumb) { ?>
                <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>"/></div>
            <?php } ?>
            <?php if ($description) { ?>
                <?php echo $description; ?>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if ($ncategories) { ?>
        <h2><?php echo $text_refine; ?></h2>
        <div class="category-list" style="border-bottom: 2px solid #eee;">
            <?php if (count($ncategories) <= 5) { ?>
                <ul>
                    <?php foreach ($ncategories as $ncategory) { ?>
                        <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <?php for ($i = 0; $i < count($ncategories);) { ?>
                    <ul>
                        <?php $j = $i + ceil(count($ncategories) / 4); ?>
                        <?php for (; $i < $j; $i++) { ?>
                            <?php if (isset($ncategories[$i])) { ?>
                                <li>
                                    <a href="<?php echo $ncategories[$i]['href']; ?>"><?php echo $ncategories[$i]['name']; ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>
<?php } ?>

    <!-- list-blog -->
<?php if ($article) { ?>

    <div class="bnews <?php if ($display_style) { ?> bnews-list-2<?php } ?>">
        <div class="row">
        <?php foreach ($article as $articles) { ?>
            <div class="col-sm-6">
                <div class="bnews-new">
                <div class="artblock<?php if ($display_style) { ?> artblock-2<?php } ?>">

                    <div class="artblock-img">
                        <?php if ($articles['thumb']) { ?>
                            <a href="<?php echo $articles['href']; ?>">
                                <img class="article-image" align="left" src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?> , <?php echo $text_photo; ?>" />
                            </a>
                        <?php } ?>
                    </div>
                    <?php if ($articles['date_added']) { ?>
                        <div class="artblock-date">
                            <?php echo $articles['date_added']; ?>
                        </div>
                    <?php } ?>
                    <?php if ($articles['name']) { ?>
                        <div class="artblock-name"><a href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a></div>
                    <?php } ?>
                    <?php if ($articles['description']) { ?>
                        <div class="description"><?php echo $articles['description']; ?></div>
                    <?php } ?>

                </div>
            </div>
            </div>
        <?php } ?>
        </div>
    </div>
    <div class="row <?php if(!$pagination) {?>marg_b_30<?php }?>">
        <div class="col-sm-12 text-center"><?php echo $pagination; ?></div>
<!--        <div class="col-sm-6 text-right">--><?php //echo $pag_results; ?><!--</div>-->
    </div>
    <script><!--
        $(document).ready(function () {
            $('img.article-image').each(function (index, element) {
                var articleWidth = $(this).parent().parent().width() * 0.7;
                var imageWidth = $(this).width() + 10;
                if (imageWidth >= articleWidth) {
                    $(this).attr("align", "center");
                    $(this).parent().addClass('bigimagein');
                }
            });
            $(window).resize(function () {
                $('img.article-image').each(function (index, element) {
                    var articleWidth = $(this).parent().parent().width() * 0.7;
                    var imageWidth = $(this).width() + 10;
                    if (imageWidth >= articleWidth) {
                        $(this).attr("align", "center");
                        $(this).parent().addClass('bigimagein');
                    }
                });
            });
        });
        //--></script>
<?php } ?>
<?php if ($is_category) { ?>
    <?php if (!$ncategories && !$article) { ?>
        <div class="content"><?php echo $text_empty; ?></div>
    <?php } ?>
<?php } else { ?>
    <?php if (!$article) { ?>
        <div class="content"><?php echo $text_empty; ?></div>
    <?php } ?>
<?php } ?>

</div>
<?php if ($disqus_status) { ?>
    <script>
        var disqus_shortname = '<?php echo $disqus_sname; ?>';
        (function () {
            var s = document.createElement('script');
            s.async = true;
            s.type = 'text/javascript';
            s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
    </script>
<?php } ?>
<?php if ($fbcom_status) { ?>
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '<?php echo $fbcom_appid; ?>',
                status: true,
                xfbml: true,
                version: 'v2.0'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php } ?>