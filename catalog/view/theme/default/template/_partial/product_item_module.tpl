<div class="product-layout">
    <div class="product-thumb transition">
        <div class="image <?php if($product['new']==1){ echo 'new_product'; }elseif($product['special']){ echo 'sale_product'; } ?>">
            <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                <?php if ($product['thumbs']) { ?>
                    <img src="<?php echo $product['thumbs']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive image_2" />
                <?php } ?>
            </a>
            <?php if ($product['price']) { ?>
                <button class="btn-product_wishlist" type="button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-heart"></i><i class="icon-heart-hover"></i></button>
            <button class="btn-product_cart" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="icon-shopping-bag"></i> <span><?php echo $button_cart; ?></span></button>
            <?php } ?>
        </div>
        <div class="caption">
            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
            <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                </p>
            <?php } ?>
        </div>
    </div>
</div>
