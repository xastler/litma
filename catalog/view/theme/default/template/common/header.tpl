<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php if($lang=='ua'){$lang='uk';} echo $lang; ?>">
<!--<![endif]-->
<!--[if lt IE 10]>
<link rel="stylesheet" href="/reject/reject.css" media="all" />
<script  src="/reject/reject.min.js"></script>
<![endif]-->

<head itemscope>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($meta) { ?>
<meta name="robots" content="<?php echo $meta; ?>"/>
<?php } ?>
<?php if (isset($robots) && $robots) { ?>
<meta name="robots" content="<?php echo $robots; ?>"/>
<?php } ?>
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />

<meta property="url" content="<?php echo $og_url; ?>" />
<meta property="title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php if ($og_image) { ?>
<meta property="image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="image" content="<?php echo $logo; ?>" />
<?php } ?>

<meta itemprop="url" content="<?php echo $og_url; ?>">
<meta itemprop="name" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php if ($og_image) { ?>
<meta itemprop="image" content="<?php echo $og_image; ?>">
<?php } else { ?>
<meta itemprop="image" content="<?php echo $logo; ?>">
<?php } ?>

    <?php if($gtm) { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?
id='+i+dl;f.parentNode.insertBefore(j,f);
 })(window,document,'script','dataLayer','<?php echo $gtm; ?>');</script>
<!-- End Google Tag Manager -->
    <?php } ?>
<?php /* посилань на альтернативні мовні версії сайту в шапці не потрібно, вони повині бути в сайтмапі лише, але покищо залишив*/?>
<?php /*foreach ($alter_lang as $lang=>$href) { ?>
    <link href="<?php echo $href; ?>" hreflang="<?php echo $lang; ?>" rel="alternate" />
<?php }*/ ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/sergo.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<link href="catalog/view/theme/default/icomoon/style.css" rel="stylesheet">
<script src="catalog/view/javascript/iscroll.js"></script>
<script src="catalog/view/javascript/main.js"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
<script src="catalog/view/javascript/jquery/selectric/js/jquery.selectric.min.js"></script>
<link href="catalog/view/javascript/jquery/selectric/selectric.css" rel="stylesheet" >
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js"></script>

<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>"></script>
<?php } ?>
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Organization",
    "url": "<?php echo $base; ?>",
    "logo": "<?php echo $logo; ?>"
}
</script>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<?php if($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                      height="0" width="0"
                      style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>

<script src="catalog/view/javascript/social_auth.js" ></script> <script> (function(d, s, id){ var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/uk_UA/sdk.js#xfbml=1&version=v2.7&appId=<?=$social_auth_facebook_app_id;?>"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk')); </script>

<div class="container">
    <div class="top_bar">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <ul class="top_bar-phone">
                    <?php if ($telephone1) { ?>
                        <li>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>">
                               <?php echo $telephone1; ?>
                            </a>
                        </li>
                    <?php }?>
                    <?php if ($telephone2) { ?>
                        <li>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">
                                <?php echo $telephone2; ?>
                            </a>
                        </li>
                    <?php }?>
                </ul>
            </div>
            <div class="col-lg-8 col-md-8">
                <?php echo $language; ?>
                <ul class="main-menu">
                    <?php if ($informations) { ?>
                        <?php foreach ($informations as $information) { ?>
                            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>
                    <?php } ?>
                    <li><a href="<?php echo $news; ?>"><?php echo $text_news; ?></a></li>
                    <li><a href="<?php echo $testimonial; ?>"><?php echo $text_testimonial; ?></a></li>
                    <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="left_bar">
    <div class="container">
        <div class="left_bar-header">
            <div class="left_bar-logo">
                <?php if ($logo) { ?>
                    <?php if ($home == $og_url) { ?>
                        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                        <p><?php echo $text_logo; ?></p>
                    <?php } else { ?>
                        <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /><p><?php echo $text_logo; ?></p></a>
                    <?php } ?>
                <?php } else { ?>
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                <?php } ?>
            </div>
            <div class="left_bar-close">
                <i class="icon-path"></i>
            </div>
        </div>
        <div class="left_bar-content">
        <ul>
            <?php if ($informations) { ?>
                <?php foreach ($informations as $information) { ?>
                    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                <?php } ?>
            <?php } ?>
            <li><a href="<?php echo $news; ?>"><?php echo $text_news; ?></a></li>
            <li><a href="<?php echo $testimonial; ?>"><?php echo $text_testimonial; ?></a></li>
            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?> </a></li>
        </ul>
    </div>
    </div>
</div>
<div class="right_bar">
    <div class="container">
    <div class="right_bar-header">
        <div class="right_bar-logo">
            <?php if ($logo) { ?>
                <?php if ($home == $og_url) { ?>
                    <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                    <p><?php echo $text_logo; ?></p>
                <?php } else { ?>
                    <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /><p><?php echo $text_logo; ?></p></a>
                <?php } ?>
            <?php } else { ?>
                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
            <?php } ?>
        </div>
        <div class="right_bar-close">
            <i class="icon-path"></i>
        </div>
    </div>
    <div class="right_bar-content">
        <ul class="right_bar-menu">
            <?php foreach ($categories as $category) { ?>
                <?php if ($category['children']) { ?>
                    <li>
                        <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                        <i class="icon-down-arrow"></i>
                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                            <ul class="right_bar-pmenu">
                                <?php foreach ($children as $child) { ?>
                                    <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>
                <?php } else { ?>
                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                <?php } ?>
            <?php } ?>
            <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
    </div>
    </div>
</div>


<header>
  <div class="container">
      <div class="header">
          <div class="header-btn_menu"><i class="icon-menu"></i></div>
          <div class="header-logo" id="logo">
              <?php if ($logo) { ?>
                <?php if ($home == $og_url) { ?>
                  <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                  <p><?php echo $text_logo; ?></p>
                <?php } else { ?>
                  <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /><p><?php echo $text_logo; ?></p></a>
                <?php } ?>
              <?php } else { ?>
                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
              <?php } ?>
          </div>
          <div class="header-btn_category"><i class="icon-menu-grid"></i></div>
          <div id="menu" class="header-catalog">
            <ul>
                <?php foreach ($categories as $category) { ?>
                    <?php if ($category['children']) { ?>
                        <li class="dropdown"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                            <div class="dropdown-menu">
                                <div class="dropdown-inner">
                                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                        <ul class="list-unstyled">
                                            <?php foreach ($children as $child) { ?>
                                                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </li>
                    <?php } else { ?>
                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                    <?php } ?>
                <?php } ?>
                <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
            </ul>
              <div id="search" class="header-search">
                  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" />
              </div>
          </div>

          <div class="header-navigation">
              <ul>
                  <li id="search-btn">
                      <a href="#">
                          <i class="icon-magnifying-glass"></i>
                      </a>
                  </li>
                  <li>
                      <a id="wishlist-total"  href="<?php echo $wishlist; ?>">
                          <i class="icon-heart"></i>
                          <span class="num-select"><?php echo $text_wishlist; ?></span>
                      </a>
                  </li>
                  <li>
                      <?php echo $cart; ?>
                  </li>
                  <li>
                      <?php if ($logged) { ?>
                          <a class="log_login" href="<?php echo $account; ?>" title="<?php echo $text_account; ?>"><i class="icon-user"></i>
                            <span><?php echo $LastName; ?><?php echo $FirstName; ?></span>
                          </a>
                      <?php }else{?>
                          <a class="quick_login">
                              <i class="icon-user"></i>
                          </a>
                      <?php }?>
                  </li>
              </ul>
          </div>
      </div>
  </div>
</header>
<?php if (0) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>
