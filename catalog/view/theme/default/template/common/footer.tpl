<footer class="footer">
  <div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="footer-info">
                <div class="footer-info-btn"><?php echo $text_info_btn; ?><i class="icon-down-arrow"></i></div>
                <ul>
                    <li>
                        <i class="icon-placeholder"></i>
                        <?php echo $address; ?>
                    </li>
                    <li>
                        <i class="icon-phone-call"></i>
                        <?php if ($telephone1) { ?>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone1) ;?>">
                                <?php echo $telephone1; ?>
                            </a>
                        <?php }?>
                        <?php if ($telephone2) { ?>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">
                                <?php echo $telephone2; ?>
                            </a>
                        <?php }?>
                        <?php if ($telephone3) { ?>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>">
                                <?php echo $telephone3; ?>
                            </a>
                        <?php }?>
                    </li>
                    <li>
                        <i class="icon-feedback"></i>
                        <span><?php echo $text_emails; ?></span>
                        <a href="mail:<?php echo $email;?>"><?php echo $email; ?></a>
                        <a href="mail:<?php echo $email2;?>"><?php echo $email2; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="footer-navigation">
                <div class="footer-menu-btn"><?php echo $text_menu_btn; ?><i class="icon-down-arrow"></i></div>
                <ul class="footer-menu">
                    <?php foreach ($categories as $category) { ?>
                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                    <?php } ?>
                    <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                </ul>
                 <?php echo $lt_newsletter; ?>
                <p><a href="<?php echo $href_download_pdf; ?>" download><i class="icon-download"></i><?php echo $text_download_prise;?></a></p>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
            <div class="footer-text">
                <p><?php echo $text_texxt; ?></p>
                <div class="footer-text-name"><?php echo $text_name_seo; ?></div>
                <div class="footer-text-img"></div>
            </div>
        </div>
    </div>
  </div>
</footer>
<div class="bottom_bar">
    <div class="container">
        <div class="bottom_bar-content">
            <div class="bottom_bar-copyright">
                <?php echo $powered; ?>
            </div>
            <ul class="bottom_bar-social">
                <li><a href="<?php echo $config_vk;?>" target="_blank"><i class="icon-facebook"></i></a></li>
                <li><a href="<?php echo $config_odn;?>" target="_blank"><i class="icon-instagram"></i></a></li>
            </ul>
            <div class="bottom_bar-company">
                <a href="https://web-systems.solutions/" target="_blank">
                    <i class="icon-group"></i>
                </a>
            </div>
        </div>
    </div>
</div>

<script type="application/ld+json">
    { "@context" : "http://schema.org",
        "@type" : "Organization",
        "name" : "<?php echo $site_name; ?>",
        "url" : "<?php echo $site_home; ?>",
        "sameAs" : [ "<?php echo $config_vk;?>",
            "<?php echo $config_odn;?>"]
    }
</script>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>