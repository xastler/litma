<?php if (count($languages) > 1) { ?>
<div class="pull-right">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
  <div class="btn-group languages">
    <button class="btn-languages dropdown-toggle" data-toggle="dropdown">
    <span class=""><?php echo $text_language; ?></span> <i class="icon-down-arrow"></i>
    </button>
    <ul class="dropdown-menu">
      <?php foreach ($languages as $language) { ?>
        <li>
            <a href="<?php echo $language['code']; ?>" class="<?php if ($language['code'] == $code) { ?>active<?php } ?>">
                <span class="l_desctop"><?php echo $language['name']; ?></span>
                <span class="l_mobal"><?php echo mb_substr($language['name'],0,3); ?></span>
            </a>
        </li>
      <?php } ?>
    </ul>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
</div>
<?php } ?>
