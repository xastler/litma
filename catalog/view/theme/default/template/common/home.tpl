<?php echo $header; ?>
<div id="content">
    <?php echo $content_top; ?>
    <div class="container">
        <div class="banner_category">
            <ul>
                <?php foreach ($categories as $category) {  ?>
                    <li>
                        <div class="banner_category-img">
                            <img src="<?php echo $category['image']; ?>" alt="">
                        </div>
                        <div class="banner_category-content">
                            <div class="banner_category-name"><?php echo $category['name']; ?></div>
                            <p><?php echo $category['description']; ?></p>
                            <a href="<?php echo $category['href']; ?>"><?php echo $text_open; ?><i class="icon-right-arrow"></i></a>
                        </div>
                        
                    </li>
                <?php } ?>
            </ul>
        </div>

        <div class="bslider_category">
                <?php foreach ($categories as $category) {  ?>
                    <div class="bslider_category-block">
                        <div class="bslider_category-img">
                            <div class="bslider_category-imgll">
                            <img src="<?php echo $category['image']; ?>" alt="">
                            </div>
                        </div>
                        <div class="bslider_category-content">
                            <div class="bslider_category-name"><?php echo $category['name']; ?></div>
                            <p><?php echo $category['description']; ?></p>
                            <a href="<?php echo $category['href']; ?>"><?php echo $text_open; ?><i class="icon-right-arrow"></i></a>
                        </div>
                    </div>
                <?php } ?>

        </div>
        <a class="btn-download" href="<?php echo $href_download_catalog; ?>" download><i class="icon-download"></i><?php echo $text_download_catalog; ?></a>
    </div>
    <?php echo $content_bottom; ?>

    <div class="merits">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <i class="icon-like"></i>
                    <p><?php echo $text_merits_1; ?></p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <i class="icon-award"></i>
                    <p><?php echo $text_merits_2; ?></p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <i class="icon-price-tag"></i>
                    <p><?php echo $text_merits_3; ?></p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <i class="icon-box"></i>
                    <p><?php echo $text_merits_4; ?></p>
                </div>
            </div>
            <div class="merits-slider">
                <div class="merits-slider-blokc">
                    <i class="icon-like"></i>
                    <p><?php echo $text_merits_1; ?></p>
                </div>
                <div class="merits-slider-blokc">
                    <i class="icon-award"></i>
                    <p><?php echo $text_merits_2; ?></p>
                </div>
                <div class="merits-slider-blokc">
                    <i class="icon-price-tag"></i>
                    <p><?php echo $text_merits_3; ?></p>
                </div>
                <div class="merits-slider-blokc">
                    <i class="icon-box"></i>
                    <p><?php echo $text_merits_4; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        $('.merits-slider').slick({
            slidesToShow:2,
            slidesToScroll: 2,
            infinite: true,
            dots:true,
            arrows:false
        });
        $('.bslider_category').slick({
            slidesToShow:1,
            slidesToScroll: 1,
            infinite: true,
            dots:true,
            arrows:false
        });
    </script>
<?php echo $footer; ?>