<div class="simplecheckout-title"><?php echo $text_header_right; ?></div>
<div class="simplecheckout-block" id="simplecheckout_cart" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>
<?php if ($display_header) { ?>
    <div class="checkout-heading panel-heading"><?php echo $text_cart ?></div>
<?php } ?>
<?php if ($attention) { ?>
    <div class="simplecheckout-warning-block"><?php echo $attention; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
    <div class="simplecheckout-warning-block"><?php echo $error_warning; ?></div>
<?php } ?>
    <div class="checkout-lists">
        <div id="scroller">
        <div class="checkout-list" id="checkout-list">
        <?php foreach ($products as $product) { ?>
            <div class="checkout-block">
                <div class="checkout-block-img">
                    <?php if ($product['thumb']) { ?>
                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                    <?php } ?>
                </div>
                <div class="checkout-block-content">
                    <div class="checkout-block-name">
                        <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        <?php if (!$product['stock'] && ($config_stock_warning || !$config_stock_checkout)) { ?>
                            <span class="product-warning">***</span>
                        <?php } ?>
                        <i class="icon-path"  data-onclick="removeGift" data-gift-key="<?php echo $voucher_info['key']; ?>" title="<?php echo $button_remove; ?>"></i>
                        <i class="icon-path" data-onclick="removeProduct" data-product-key="<?php echo !empty($product['cart_id']) ? $product['cart_id'] : $product['key'] ?>" data-toggle="tooltip">
                        </i>
                    </div>
                    <ul class="checkout-block-info">
                        <li><span><?php echo $text_article; ?>: </span><?php echo !empty($product['sku'])? $product['sku'] : $product['model']; ?></li>
                        <?php foreach ($product['option'] as $option) { ?>
                        <li><span><?php echo $option['name']; ?>: </span><?php echo $option['value']; ?></li>
                        <?php } ?>
                    </ul>
                    <div class="checkout-block-price">
                        <span class="price-main"><?php echo $product['price']; ?></span>
                        <div class="checkout-block-quantity" >
                            <button class="minus" data-onclick="decreaseProductQuantity" data-toggle="tooltip" type="submit">
                                <i class="icon-down-arrow"></i>
                            </button>
                            <input class="form-control" type="text" data-onchange="changeProductQuantity" name="quantity[<?php echo !empty($product['cart_id']) ? $product['cart_id'] : $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />
                            <button class="plus" data-onclick="increaseProductQuantity" data-toggle="tooltip" type="submit">
                                <i class="icon-down-arrow"></i>
                            </button>
                        </div>
                        <span class="price-total"><?php echo $product['total']; ?></span>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    </div>
    </div>
<?php foreach ($totals as $total) { ?>
    <div class="simplecheckout-cart-total total_<?php echo $total['code']; ?>" id="total_<?php echo $total['code']; ?>">
        <span><?php echo $total['title']; ?>:</span>
        <span class="simplecheckout-cart-total-value"><?php echo $total['text']; ?></span>
    </div>
<?php } ?>
<div class="checkout-buttons">
    <div class="checkout-prev btn-default-x"><?php echo $text_btn_prev; ?></div>
    <div class="checkout-order btn-default"><?php echo $text_btn_order; ?></div>
</div>
<?php if (isset($modules['coupon'])) { ?>
    <div class="simplecheckout-cart-total">
        <span class="inputs"><?php echo $entry_coupon; ?>&nbsp;<input class="form-control" type="text" data-onchange="reloadAll" name="coupon" value="<?php echo $coupon; ?>" /></span>
    </div>
<?php } ?>
<?php if (isset($modules['reward']) && $points > 0) { ?>
    <div class="simplecheckout-cart-total">
        <span class="inputs"><?php echo $entry_reward; ?>&nbsp;<input class="form-control" type="text" name="reward" data-onchange="reloadAll" value="<?php echo $reward; ?>" /></span>
    </div>
<?php } ?>
<?php if (isset($modules['voucher'])) { ?>
    <div class="simplecheckout-cart-total">
        <span class="inputs"><?php echo $entry_voucher; ?>&nbsp;<input class="form-control" type="text" name="voucher" data-onchange="reloadAll" value="<?php echo $voucher; ?>" /></span>
    </div>
<?php } ?>

<input type="hidden" name="remove" value="" id="simplecheckout_remove">
<div style="display:none;" id="simplecheckout_cart_total"><?php echo $cart_total ?></div>
<?php if ($display_weight) { ?>
    <div style="display:none;" id="simplecheckout_cart_weight"><?php echo $weight ?></div>
<?php } ?>
<?php if (!$display_model) { ?>
    <style>
    .simplecheckout-cart col.model,
    .simplecheckout-cart th.model,
    .simplecheckout-cart td.model {
        display: none;
    }
    </style>
<?php } ?>
</div>
<script>
    var myScroll;
    function scroll() {
        myScroll = new IScroll('.checkout-lists ', {
            bounce: false,
            scrollbars: true,
            mouseWheel: true,
            interactiveScrollbars: true,
            mouseWheelSpeed: 120
        });
    }
    if($('.checkout-lists').height()>=1021){
        scroll();
    }
</script>