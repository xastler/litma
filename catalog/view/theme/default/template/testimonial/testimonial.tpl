<?php echo $header; ?>
<ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
    <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <?php if ($i + 1 < count($breadcrumbs)) { ?>
                <a itemscope itemtype="http://schema.org/Thing" itemprop="item"
                   href="<?php echo $breadcrumb['href']; ?>">
                    <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
            <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
            <?php } ?>
        </li>
    <?php } ?>
</ul>
<div class="mega_banner_testimonial">

    <h1><?php echo $heading_title; ?></h1>
</div>
    <div class="container">

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <form id="form-review">
                <?php if ($review_status) { ?>
                    <div id="review"></div>
                    <?php if ($review_guest) { ?>
                        <div class="testimonial-form">
                        <h2><?php echo $text_write; ?></h2>
                        <div class="form-inputs">
                            <div class="form-input required">
                                <input type="text" name="name" value="" id="input-name" placeholder="<?php echo $entry_name; ?>"/>
                            </div>
                            <div class="form-input required">
                                <input type="text" name="email" value="" id="input-email" placeholder="<?php echo $entry_email; ?>"/>
                            </div>
                            <div class="form-rating required">
                                <p><?php echo $entry_rating; ?></p>
                                <div class="form-rating-star">
                                    <input id="star-5" type="radio" name="rating" value="5">
                                    <label for="star-5" class="icon-star-grey"></label>

                                    <input id="star-4" type="radio" name="rating" value="4">
                                    <label for="star-4" class="icon-star-grey"></label>

                                    <input id="star-3" type="radio" name="rating" value="3">
                                    <label for="star-3" class="icon-star-grey"></label>

                                    <input id="star-2" type="radio" name="rating" value="2">
                                    <label for="star-2" class="icon-star-grey"></label>

                                    <input id="star-1" type="radio" name="rating" value="1">
                                    <label for="star-1" class="icon-star-grey"></label>

                                </div>
                            </div>
                        </div>
                        <div class="form-textarea">
                            <textarea name="text" rows="5" id="input-review" placeholder="<?php echo $entry_review; ?>"></textarea>
                        </div>
                        <div class="buttons clearfix">
                            <button type="button" id="button-review"
                                    data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_continue   ; ?></button>
                        </div>


                    <?php } else { ?>
                     <?php echo $text_login; ?>
                    <?php } ?>
                    </div>
                <?php } ?>
            </form>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>

    <script ><!--
        $('#review').delegate('.pagination a', 'click', function (e) {
            e.preventDefault();
            $('#review').load(this.href);
        });

        $('#review').load('<?php echo html_entity_decode($review); ?>');

        $('#button-review').on('click', function () {
            $.ajax({
                url: '<?php echo html_entity_decode($write); ?>',
                type: 'post',
                dataType: 'json',
                data:  $("#form-review").serialize(),
                beforeSend: function () {
                    if ($("textarea").is("#g-recaptcha-response")) {
                        grecaptcha.reset();
                    }
                    $('#button-review').button('loading');
                },
                complete: function () {
                    $('#button-review').button('reset');
                },
                success: function (json) {
                    $('.alert-success, .alert').remove();
                    if (json['error']) {
                        $('#form-review textarea').after('<div class="alert"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }
                    if (json['success']) {

                        $('.header').before('<div class="popups_block"><i class="fa fa-check-circle"></i> ' + json['success']);

                        $('input[name=\'name\']').val('');
                        $('textarea[name=\'text\']').val('');
                        $('input[name=\'rating\']:checked').prop('checked', false);
                    }
                }
            });
        });
        //--></script>
</div>
<?php echo $footer; ?>