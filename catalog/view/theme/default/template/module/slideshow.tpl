<div class="slideshow">
    <div class="slideshow-prev">
        <i class="icon-right-arrow"></i>
    </div>
    <div class="container">
        <div id="slideshow<?php echo $module; ?>" >
          <?php foreach ($banners as $banner) { ?>
          <div class="item">
            <?php if ($banner['link']) { ?>
                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                <p><?php echo $banner['title']; ?></p>
                <a href="<?php echo $banner['link']; ?>"><span><?php echo $text_link; ?></span><i class="icon-right-arrow"></i></a>
            <?php } else { ?>
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                <p><?php echo $banner['title']; ?></p>
            <?php } ?>
          </div>
          <?php } ?>
        </div>
    </div>
    <div class="slideshow-next">
        <i class="icon-right-arrow"></i>
    </div>
</div>
<script>
$('#slideshow<?php echo $module; ?>').slick({
    dots: true,
    infinite: true,
    speed: 300,
    prevArrow: $('.slideshow-prev'),
    nextArrow: $('.slideshow-next'),
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2500,
    responsive: [
        {
            breakpoint: 1300,
            settings: {
                arrows: false
            }
        }
    ]
});
</script>