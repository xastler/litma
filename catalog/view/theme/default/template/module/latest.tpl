<div class="module_latest">
    <div class="container">
        <h2><?php echo $heading_title; ?></h2>
        <div class="module_latest-slider">
            <div class="module_latest-prev">
                <i class="icon-right-arrow"></i>
            </div>
            <div class="module_latest-slide">
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
                <?php } ?>
            </div>
            <div class="module_latest-next">
                <i class="icon-right-arrow"></i>
            </div>
        </div>
    </div>
</div>



<script>
    $('.module_latest-slide').slick({
        dots: true,
        infinite: true,
        speed: 300,
        prevArrow: $('.module_latest-prev'),
        nextArrow: $('.module_latest-next'),
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true,
                    arrows: false
                }
            },
            {
                breakpoint: 551,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true,
                    arrows: false
                }
            }
        ]
    });
</script>
