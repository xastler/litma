
<?php if(isset($products)){ ?>

    <div class="watched_products">
        <h2 class="title_slider-pro"><?php echo $heading_title; ?></h2>
        <div class="module_special-slider">
            <div class="module_special-prev">
                <i class="icon-right-arrow"></i>
            </div>
            <div class="module_special-slide">
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'button_details' => $button_details));?>
                <?php } ?>
            </div>
            <div class="module_special-next">
                <i class="icon-right-arrow"></i>
            </div>
        </div>
    </div>
<?php } ?>
<script>
    $('.module_special-slide').slick({
        dots: true,
        infinite: true,
        speed: 300,
        prevArrow: $('.module_special-prev'),
        nextArrow: $('.module_special-next'),
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true,
                    arrows: false
                }
            },
            {
                breakpoint: 551,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true,
                    arrows: false
                }
            }
        ]
    });
</script>
