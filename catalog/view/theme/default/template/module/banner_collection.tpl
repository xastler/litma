<div class="banner_collection">
    <ul>
        <?php foreach ($categories as $category) {  ?>
            <?php foreach ($category['children'] as $child) {  ?>
                <li>
                    <a href="<?php echo $child['href']; ?>">
                        <img src="<?php echo $child['image']; ?>" alt="">
                        <div class="banner_collection-content">
                            <div class="banner_collection-title"><?php echo $category['name']; ?></div>
                            <div class="banner_collection-name"><?php echo $child['name']; ?></div>
                        </div>
                    </a>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
</div>
<div class="container">
    <div class="banner_collection-slider">
        <?php foreach ($categories as $category) {  ?>
            <?php foreach ($category['children'] as $child) {  ?>
                <div class="banner_collection-slider-block">
                    <a href="<?php echo $child['href']; ?>">
                        <img src="<?php echo $child['image']; ?>" alt="">
                        <div class="banner_collection-content">
                            <div class="banner_collection-title"><?php echo $category['name']; ?></div>
                            <div class="banner_collection-name"><?php echo $child['name']; ?></div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>

<script>
    $('.banner_collection-slider').slick({
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    dots: true,
                    infinite: true,
                    speed: 300,
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>