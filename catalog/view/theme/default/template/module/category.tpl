<?php if(34 == $category_id){ ?>
<div class="catalog_collection">
<ul class="catalog_collection-list">
  <?php $class_active; foreach ($collections as $collection) { ?>
      <?php if($collection['category_id'] == $category_id){$class_active='active'; }else{$class_active=' ';}?>
      <li>
          <a href="<?php echo $collection['href']; ?>"><?php echo $collection['name']; ?>  </a>
          <?php if ($collection['children']) { ?>
              <ul class="catalog_collection-list-lvl">
              <?php foreach ($collection['children'] as $child) { ?>
                  <?php if ($child['category_id'] == $child_id) { ?>
                    <li><a href="<?php echo $child['href']; ?>" class="active"><?php echo $child['name']; ?></a></li>
                  <?php } else { ?>
                    <li><a href="<?php echo $child['href']; ?>" class=""><?php echo $child['name']; ?></a></li>
                  <?php } ?>
              <?php } ?>
              </ul>
          <?php } ?>
      </li>
  <?php } ?>
</ul>
</div>
<?php } else{?>
    <div class="catalog_category">
        <h2><?php echo $heading_title; ?> <i class="icon-down-arrow"></i></h2>
        <ul class="catalog_category-list">
            <?php $class_active; foreach ($categories as $category) { ?>
                <?php if($category['category_id'] == $category_id){$class_active='active'; }else{$class_active=' ';}?>
                <li>
                    <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?>  </a>
                    <?php if ($category['children']) { ?><i class="icon-down-arrow <?php echo $class_active;?>"></i>
                        <ul class="catalog_category-list-lvl <?php echo $class_active;?>">
                            <?php foreach ($category['children'] as $child) { ?>
                                <?php if ($child['category_id'] == $child_id) { ?>
                                    <li><a href="<?php echo $child['href']; ?>" class="active"><?php echo $child['name']; ?></a></li>
                                <?php } else { ?>
                                    <li><a href="<?php echo $child['href']; ?>" class=""><?php echo $child['name']; ?></a></li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
            <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
    </div>
<?php } ?>
