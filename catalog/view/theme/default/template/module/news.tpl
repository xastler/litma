<?php echo $banner_collection; ?>

<div class="bnews">
    <div class="container">
        <h2><?php echo $heading_title; ?></h2>
        <div class="bnews-content">
            <div class="bnews-new">
                <?php $i1 = 0;
                foreach ($article as $articles) { ?>
                    <?php if ($i1 < 1) { ?>
                        <div class="artblock<?php if ($display_style) { ?> artblock-2<?php } ?>">
                            <div class="artblock-img">
                                <?php if ($articles['thumb']) { ?>
                                    <a href="<?php echo $articles['href']; ?>"><img class="article-image"
                                                                                    src="<?php echo $articles['thumb']; ?>"
                                                                                    title="<?php echo $articles['name']; ?>"
                                                                                    alt="<?php echo $articles['name']; ?>, <?php echo $text_photo; ?>"/></a>
                                <?php } ?>
                            </div>
                            <?php if ($articles['date_added']) { ?>
                                <div class="artblock-date">
                                    <?php echo $articles['date_added']; ?>
                                </div>
                            <?php } ?>
                            <?php if ($articles['name']) { ?>
                                <div class="artblock-name"><a
                                            href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a>
                                </div>
                            <?php } ?>
                            <?php if ($articles['description']) { ?>
                                <div class="description"><?php echo $articles['description']; ?></div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php $i1++;
                } ?>
            </div>
            <div class="bnews-list">
                <?php $i2 = 0;
                foreach ($article as $articles) { ?>
                    <?php if ($i2 >= 1) { ?>
                        <div class="artblock<?php if ($display_style) { ?> artblock-2<?php } ?>">
                            <?php if ($articles['date_added']) { ?>
                                <div class="artblock-date">
                                    <?php echo $articles['date_added']; ?>
                                </div>
                            <?php } ?>
                            <?php if ($articles['name']) { ?>
                                <div class="artblock-name"><a
                                            href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a>
                                </div>
                            <?php } ?>
                            <?php if ($articles['description']) { ?>
                                <div class="artblock-description"><?php echo $articles['description']; ?></div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php $i2++;
                } ?>
            </div>
        </div>
        <a class="bnews-btn" href="<?php echo $news; ?>"><span><?php echo $button_more; ?></span><i
                    class="icon-right-arrow"></i></a>
    </div>
</div>

<script><!--
    $(document).ready(function () {
        $('img.article-image').each(function (index, element) {
            var articleWidth = $(this).parent().parent().width() * 0.7;
            var imageWidth = $(this).width() + 10;
            if (imageWidth >= articleWidth) {
                $(this).attr("align", "center");
                $(this).parent().addClass('bigimagein');
            }
        });
    });
    //--></script>
<?php if ($disqus_status) { ?>
    <script>
        var disqus_shortname = '<?php echo $disqus_sname; ?>';
        (function () {
            var s = document.createElement('script');
            s.async = true;
            s.type = 'text/javascript';
            s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
    </script>
<?php } ?>
<?php if ($fbcom_status) { ?>
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '<?php echo $fbcom_appid; ?>',
                status: true,
                xfbml: true,
                version: 'v2.0'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php } ?>