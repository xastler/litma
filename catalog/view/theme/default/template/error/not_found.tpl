<?php echo $header; ?>
<ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <?php if($i+1<count($breadcrumbs)) { ?>
                <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                    <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
            <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
            <?php } ?>
        </li>
    <?php } ?>
</ul>
<div class="mega_banner_404">
    <?php /*<h1><?php echo $heading_title; ?></h1>*/?>
    <h1>404</h1>
    <p><?php echo $text_error; ?></p>
    <a href="<?php echo $continue; ?>"><?php echo $text_button_error; ?><i class="icon-right-arrow"></i></a>
</div>
<?php echo $footer; ?>