<?php
class ControllerFeedGoogleSitemapMultiLanguages extends Controller {

    private $code_lan = array();//масив активних мов
    private $output = '';       //результат sitemap
    private $server = '';       //домен сайту

    public function index() {
		if ($this->config->get('google_sitemap_multi_languages_status')) {
			/*echo '<?xml version="1.0" encoding="UTF-8"?>';*/
			$this->output = '<?xml version="1.0" encoding="utf-8"?>';
			$this->output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

			$this->load->model('catalog/product');
			$this->load->model('tool/image');

            $this->session->data['language'] = $this->config->get('config_language_default');
            $this->load->model('localisation/language');
            $languages = $this->model_localisation_language->getLanguages();
            foreach ($languages as $result) {
                if ($result['status']) {
                    //$code = $result['code'];
                    //$code  = strtok($code, '-');якщо код мови вказується через "-", було на арті так
                    $this->code_lan[$result['code']] = $result['code'].'/';
                }
            }
            if ($this->request->server['HTTPS']) {
                $this->server = $this->config->get('config_ssl');
            } else {
                $this->server = $this->config->get('config_url');
            }
            $lang_ssl = $this->request->server['HTTPS'];

            /* головна сторінка*/
            if(1){
                $lang_route = 'common/home';
                $lang_query = '';
                $lang_url = $this->get_lang_url($lang_route,$lang_query,$lang_ssl);
                foreach($lang_url as $key => $url){
                    $this->output .= '<url>';
                    $this->output .= '<loc>' . $url . '</loc>';
                    $this->print_alternate_url($lang_url);
                    $this->output .= '<changefreq>daily</changefreq>';
                    $this->output .= '<priority>1.0</priority>';
                    $this->output .= '</url>';
                }
            }

            /*Продукти*/
            if(1){
                $products = $this->model_catalog_product->getProducts();
                foreach ($products as $product) {
                    $lang_route = 'product/product';
                    $lang_query = 'product_id=' . $product['product_id'];
                    $lang_url = $this->get_lang_url($lang_route,$lang_query,$lang_ssl);
                    //$url = $this->url->link($lang_route,$lang_query,$lang_ssl);
                    //if (!preg_match('|index.php|',$url)) {
                    foreach($lang_url as $key => $url){
                        $this->output .= '<url>';
                        $this->output .= '<loc>' . $url . '</loc>';
                        $this->print_alternate_url($lang_url);
                        $this->output .= '<changefreq>weekly</changefreq>';
                        $this->output .= '<lastmod>' . date('Y-m-d\TH:i:sP', strtotime($product['date_modified'])) . '</lastmod>';
                        $this->output .= '<priority>0.9</priority>';
                        if($product['image']){
                            $this->output .= '<image:image>';
                            $this->output .= '<image:loc>' . str_replace('&', '&amp;', $this->model_tool_image->resize($product['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'))) . '</image:loc>';
                            $this->output .= '<image:caption>' . str_replace('&', '&amp;', $product['name']) . '</image:caption>';
                            $this->output .= '<image:title>' . str_replace('&', '&amp;', $product['name']) . '</image:title>';
                            $this->output .= '</image:image>';
                        }
                        $this->output .= '</url>';
                    }
                    //}
                }
            }

            /*Категорії*/
            if(1){
                $this->load->model('catalog/category');
                $this->getCategories(0);
            }

            /*Виробники*/
            if(1){
                $this->load->model('catalog/manufacturer');
                $manufacturers = $this->model_catalog_manufacturer->getManufacturers();
                foreach ($manufacturers as $manufacturer) {
                    $lang_route = 'product/manufacturer/info';
                    $lang_query = 'manufacturer_id=' . $manufacturer['manufacturer_id'];
                    $lang_url = $this->get_lang_url($lang_route,$lang_query,$lang_ssl);
                    foreach($lang_url as $key => $url){
                        $this->output .= '<url>';
                        $this->output .= '<loc>' . $url . '</loc>';
                        $this->print_alternate_url($lang_url);
                        $this->output .= '<changefreq>weekly</changefreq>';
                        $this->output .= '<priority>0.9</priority>';
                        $this->output .= '</url>';
                    }
                    $products = $this->model_catalog_product->getProducts(array('filter_manufacturer_id' => $manufacturer['manufacturer_id']));
                    foreach ($products as $product) {
                        $lang_route = 'product/product';
                        $lang_query = 'manufacturer_id=' . $manufacturer['manufacturer_id'] . '&product_id=' . $product['product_id'];
                        $lang_url = $this->get_lang_url($lang_route,$lang_query,$lang_ssl);
                        foreach($lang_url as $key => $url){
                            $this->output .= '<url>';
                            $this->output .= '<loc>' . $url . '</loc>';
                            $this->print_alternate_url($lang_url);
                            $this->output .= '<changefreq>weekly</changefreq>';
                            $this->output .= '<priority>1.0</priority>';
                            $this->output .= '</url>';
                        }
                    }
                }
            }

            /*інформаційні сторінки*/
            if(1){
                $this->load->model('catalog/information');
                $informations = $this->model_catalog_information->getInformations();

                foreach ($informations as $information) {
                    $lang_route = 'information/information';
                    $lang_query = 'information_id=' . $information['information_id'];
                    //$url = $this->url->link($lang_route,$lang_query,$lang_ssl);
                    $lang_url = $this->get_lang_url($lang_route,$lang_query,$lang_ssl);
                    //if (!preg_match('|index.php|',$url)){
                    foreach($lang_url as $key => $url){
                        $this->output .= '<url>';
                        $this->output .= '<loc>' . $url . '</loc>';
                        $this->print_alternate_url($lang_url);
                        $this->output .= '<changefreq>weekly</changefreq>';
                        $this->output .= '<priority>0.5</priority>';
                        $this->output .= '</url>';
                    }
                    //}
                }
            }

			$this->output .= '</urlset>';
            header('Content-type: text/xml; charset="utf-8"');
			$this->response->addHeader('Content-Type: application/xml');
			$this->response->setOutput($this->output);
		}
	}

	private function getCategories($parent_id, $current_path = '') {

        $this->session->data['language'] = $this->config->get('config_language_default');
        $lang_ssl = $this->request->server['HTTPS'];
		$results = $this->model_catalog_category->getCategories($parent_id);

		foreach ($results as $result) {
			if (!$current_path) {
				$new_path = $result['category_id'];
			} else {
				$new_path = $current_path . '_' . $result['category_id'];
			}
            $lang_route = 'product/category';
            $lang_query = 'path=' . $new_path;
            //$url = $this->url->link($lang_route,$lang_query,$lang_ssl);
            $lang_url = $this->get_lang_url($lang_route,$lang_query,$lang_ssl);
            //if (!preg_match('|index.php|', $url)) {
            foreach($lang_url as $key => $url){
                $this->output .= '<url>';
                $this->output .= '<loc>' . $url . '</loc>';
                $this->print_alternate_url($lang_url);
                $this->output .= '<changefreq>weekly</changefreq>';
                $this->output .= '<priority>0.9</priority>';
                $this->output .= '</url>';
            }
            //}
			$products = $this->model_catalog_product->getProducts(array('filter_category_id' => $result['category_id']));

			foreach ($products as $product) {
			    $lang_route = 'product/category';
                $lang_query = 'path=' . $new_path . '&product_id=' . $product['product_id'];
                //$url = $this->url->link($lang_route,$lang_query,$lang_ssl);
                $lang_url = $this->get_lang_url($lang_route,$lang_query,$lang_ssl);
                //if(!preg_match('|index.php|', $url)) {
                foreach($lang_url as $key => $url){
                    $this->output .= '<url>';
                    $this->output .= '<loc>' . $url . '</loc>';
                    $this->print_alternate_url($lang_url);
                    $this->output .= '<changefreq>weekly</changefreq>';
                    $this->output .= '<priority>0.9</priority>';
                    $this->output .= '</url>';
                }
                //}
			}

			$this->getCategories($result['category_id'], $new_path);
		}

		return $this->output;
	}

    /**
     * повертає мовний масив альтернативних адрес сторінки
     * @param $lang_route - route
     * @param $lang_query - query
     * @param $lang_ssl - SSL
     */
	private function get_lang_url($lang_route,$lang_query,$lang_ssl){
        $url = $this->url->link($lang_route,$lang_query,$lang_ssl);
        $url_short = str_replace($this->server,'',$url);
        $lang_url = array();
        foreach($this->code_lan as $key => $lang_code){
            if($key == $this->config->get('config_language_default')){
                $lang_url[$key] = $this->server.$url_short;
            }else{
                $lang_url[$key] = $this->server.$lang_code.$url_short;
            }
        }
        return $lang_url;
    }

    /**
     * виводимо масив альтернативних адрес сторінки
     * @param $lang_url - масив альтернативних адрес сторінки
     */
    private function print_alternate_url($lang_url){
	    foreach($lang_url as $key => $url){
            $this->output .= '<xhtml:link rel="alternate" hreflang="'.$key.'" href="' . $url . '"/>';
        }
    }
}
