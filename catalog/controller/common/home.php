<?php
class ControllerCommonHome extends Controller {
	public function index() {

		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

         $this->config->get('config_meta_title');


		if (isset($this->request->get['route'])) {
			$this->document->addLink(((isset($this->request->server['HTTPS']) && !empty($this->request->server['HTTPS'])) ? HTTPS_SERVER : HTTP_SERVER), 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['banner_collection'] = $this->load->controller('module/banner_collection');

        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $data['categories'] = array();

        $this->load->language('common/home');
        $data['text_download_catalog'] = $this->language->get('text_download_catalog');
        $data['text_open'] = $this->language->get('text_open');
        $data['text_merits_1'] = $this->language->get('text_merits_1');
        $data['text_merits_2'] = $this->language->get('text_merits_2');
        $data['text_merits_3'] = $this->language->get('text_merits_3');
        $data['text_merits_4'] = $this->language->get('text_merits_4');

        $data['href_download_catalog'] = HTTP_IMAGE . $this->config->get('config_catalog_file');

        $categories = $this->model_catalog_category->getCategories(0);
        $k=1;
        foreach ($categories as $category) {
            if ($k<=3) {
                $k++;
                // Level 1
                if ($category['image']) {
                    $image = $this->model_tool_image->myresize($category['image'],  487, 300);
                } else {
                    $image = $this->model_tool_image->myresize('no_image.png' , 487, 300);
                }
                $data['categories'][] = array(
                    'name'     => $category['name'],
                    'description'   => html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8'),
                    'image'   => $image,
                    'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}
}
