<?php

class ControllerModuleLatest extends Controller
{
    public function index($setting)
    {
        $this->load->language('module/latest');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_tax'] = $this->language->get('text_tax');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['products'] = array();

        $filter_data = array(
            'sort' => 'p.date_added',
            'order' => 'DESC',
			'start' => 0,
			'filter_new' => 1,
			'limit' => $setting['limit']
        );

        $results = $this->model_catalog_product->getProducts($filter_data);

        if ($results) {
            foreach ($results as $result) {
                if ($result['new'] == 1) {
                    $images=false;

                    $results_image = $this->model_catalog_product->getProductImages($result['product_id']);

                    $i=0;
                    foreach  ($results_image as $result_image) {
                        if($i<1){
                            $images = $this->model_tool_image->resize($result_image['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                            $i++;
                        }
                    }
                    if ($result['image']) {
                        $image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if ((float)$result['special']) {
                        $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $result['rating'];
                    } else {
                        $rating = false;
                    }

                    $data['products'][] = array(
                        'product_id' => $result['product_id'],
                        'thumb' => $image,
                        'thumbs' => $images,
                        'name' => $result['name'],
                        'new' => $result['new'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price' => $price,
                        'special' => $special,
                        'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'tax' => $tax,
                        'rating' => $rating,
                        'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                    );
                }
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/latest.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/module/latest.tpl', $data);
            } else {
                return $this->load->view('default/template/module/latest.tpl', $data);
            }
        }
    }
}
