<?php
class ControllerModuleBannerCollection extends Controller {
	public function index() {

        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);
        $k=1;
        foreach ($categories as $category) {
            // Level 1
            if ($k==4) {
                $children_data = array();


                    $children = $this->model_catalog_category->getCategories($category['category_id']);

                    foreach($children as $child) {
                        // Level 1

                        if ($child['image']) {
                            $image = $this->model_tool_image->resize($child['image'],  945, 500);
                        } else {
                            $image = $this->model_tool_image->resize('no_image.png' , 945, 500);
                        }
                        $children_data[] = array(
                            'name'     => $child['name'],
                            'description'   => $child['description'],
                            'image'   => $image,
                            'href'     => $this->url->link('product/category', 'path=' . $child['category_id'])
                        );
                    }

                $data['categories'][] = array(
                    'name'        => $category['name'],
                    'children'    => $children_data
                );
            }
            $k++;
        }


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/banner_collection.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/banner_collection.tpl', $data);
		} else {
            return $this->load->view('default/template/module/banner_collection.tpl', $data);
		}
	}
}
