<?php
// Heading
$_['heading_title'] = '404';

// Text
$_['text_error']    = 'Error. Page not found!';

//button
$_['text_button']   = 'Home page';