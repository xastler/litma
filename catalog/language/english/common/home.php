<?php
// Text
$_['text_download_catalog']   =   'Download';
$_['text_open']               =   'review';
$_['text_merits_1']           =   'Modern equipment';
$_['text_merits_2']           =   'High quality';
$_['text_merits_3']           =   'Affordable prices';
$_['text_merits_4']           =   'Free shipping';
