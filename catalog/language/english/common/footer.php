<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contacts';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'SALE';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = '&copy; Ltd. “%s” 2015-%s. All rights reserved ';

$_['text_texxt']        = '„We will be glad to cooperate with you, taking into account your wishes and commercial interest“';
$_['text_name_seo']     = '<span>Michalsky V.I.</span><br>General Director of “Litma” Ltd.';
$_['text_emails']        = 'Sales department:';
$_['text_download_prise']     = 'DOWNLOAD PRICE LIST';

$_['entry_name']      	      = 'Name';
$_['entry_phone']             = 'Phone';
$_['text_call']      	      = 'Order a call';
$_['text_send']      	      = 'Send';
$_['text_loading']            = 'Workout';
$_['text_info_btn']           = 'contacts';
$_['text_menu_btn']           = 'Catalog';