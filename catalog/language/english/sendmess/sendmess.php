<?php
// Text
$_['text_name']  			     = 'Name:';
$_['text_email']  				 = 'E-mail:';
$_['text_message'] 				 = 'Message:';
$_['text_phone']				 = 'Phone:';

$_['error_name']   				 = 'The name must be from 3 to 25 characters!';
$_['error_email']   			 = 'E-mail is required!';
$_['error_message'] 			 = 'The message must be from 10 characters!';
$_['error_phone'] 			 	 = 'The phone must be from 5 characters!';
$_['success']      				 = 'Your request has been sent successfully. We will contact you shortly!';

$_['email_subject_message'] 	 = 'A new message';
$_['email_subject_callback'] 	 = 'New call order';
