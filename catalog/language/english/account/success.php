<?php
// Heading
$_['heading_title'] = 'Your Account Has Been Created!';

// Text
$_['text_message']  = '<p>Congratulations! Your new account has been successfully created!</p><p>If you have ANY questions about the operation of this online shop, please e-mail the store owner.</p> <p>A confirmation has been sent to the provided e-mail address. If you have not received it within the hour, please <a href="%s">contact us</a>.</p>';
$_['text_approval'] = '<p>Thank you for registering with %s!</p><p>Activate your account by clicking the link in the message that was sent to your email.</p><p>If you have ANY questions about the operation of this online shop, please <a href="%s">contact the store owner</a>.</p>';
$_['text_account']  = 'Account';
$_['text_success']  = 'Success';

$_['text_success_approved'] = 'The account has been successfully verified!';
$_['text_message_approved'] = '<p> Congratulations! Your Account has been successfully verified. </p>  <p> If you have Any questions, write to us. </ p> <p> A letter with the registration data was sent to the specified E-mail. If you have not received a letter, please <a href="%s"> contact us </a>. </ p> ';
$_['text_success_approved_false'] = 'The account has NOT been verified!';
$_['text_message_approved_false'] = '<p> Your account has NOT been verified. </ p> <p> If you have any questions, please <a href="%s"> contact the administrator </a>. </ p> ';
