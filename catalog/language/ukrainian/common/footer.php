<?php
// Text
$_['text_information']        = 'Інформація';
$_['text_service']            = 'Служба підтримки';
$_['text_extra']              = 'Додатково';
$_['text_contact']            = 'Зв’язатися з нами';
$_['text_return']             = 'Повернення товару';
$_['text_sitemap']            = 'Мапа сайту';
$_['text_manufacturer']       = 'Виробники';
$_['text_voucher']            = 'Подарункові сертифікати';
$_['text_affiliate']          = 'Партнери';
$_['text_special']            = 'SALE';
$_['text_account']            = 'Особистий кабінет';
$_['text_order']              = 'Історія замовлень';
$_['text_wishlist']           = 'Мої закладки';
$_['text_newsletter']         = 'Розсилка новин';
$_['text_powered']            = '&copy; ТОВ “%s” 2015-%s. Всі права захищено ';
$_['text_texxt']              = '„Ми будемо раді з Вами співпрацювати, враховуючи Ваші побажання і комерційний інтерес“';
$_['text_emails']             = 'Відділ продажу:';
$_['text_download_prise']     = 'ЗАВАНТАЖИТИ ПРАЙС-ЛИСТ';
$_['text_name_seo']           = '<span>Михальський В.І.</span><br>Генеральний директор ТОВ “Літма”';

$_['entry_name']      	      = 'Ім\'я';
$_['entry_phone']             = 'Телефон';
$_['text_call']      	      = 'Замовити дзвінок';
$_['text_send']      	      = 'Відправити';
$_['text_loading']            = 'Опрацювання';
$_['text_info_btn']           = 'контакти';
$_['text_menu_btn']           = 'Каталог';
