<?php
// Text
$_['text_signin_register']    = 'Sign In/Register';
$_['text_login']   			  = 'Sign In';
$_['text_register']    		  = 'Реєстрація';

$_['text_new_customer']       = 'New Customer';
$_['text_returning']          = 'Авторизація';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'Name';
$_['entry_password']          = 'Пароль';
$_['entry_telephone']         = 'Telephone';
$_['text_forgotten']          = 'Забули пароль?';
$_['text_agree']              = 'I agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['text_enter_social']              = 'Ввійти за допомогою:';



//Button
$_['button_login']            = 'Ввійти';

//Error
$_['error_name'] = 'Ім\'я має містити від 1 до 32 символів!';
$_['error_email'] = 'Адрес електронної пошти не вважається дійсним!';
$_['error_telephone'] = 'Телефон має містити від 3 до 32 символів!';
$_['error_password'] = 'Пароль має містити від 4 до 20 символів!';
$_['error_exists'] = 'Попередження: адреса електронної пошти вже зареєстрована!';
$_['error_agree'] = 'Попередження: ви повинні погодитися з %s!';
$_['error_warning'] = 'Попередження: уважно перевірте форму для помилок!';
$_['error_approved'] = 'Попередження: ваш обліковий запис вимагає підтвердження, перш ніж ви зможете увійти в систему.';
$_['error_login'] = 'Попередження: не підходить для адреси електронної пошти та / або пароля.';