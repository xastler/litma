<?php
// Text
$_['heading_title']	           = 'Відгуки';

$_['text_write']               = 'Залишити відгук';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Дякую за відгук. Він був переданий веб-майстру для затвердження.';

$_['text_mail_subject']        = 'You have a new testimonial (%s).';
$_['text_mail_waiting']	       = 'You have a new testimonial waiting.';
$_['text_mail_author']	       = 'Author: %s';
$_['text_mail_rating']	       = 'Rating: %s';
$_['text_mail_text']	       = 'Text:';

// Entry
$_['entry_name']               = 'Ім’я';
$_['entry_review']             = 'Повідомлення';
$_['entry_email']              = 'Email';
$_['entry_rating']             = 'Рейтинг';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Button
$_['button_continue']          = 'Відправити';

// Error
$_['error_name']               = 'Ім\'я відгуку має бути від 3 до 25 символів!';
$_['error_text']               = 'Текст відгуку повинен бути від 25 до 3000 символів';
$_['error_rating']             = 'Потрібно виставити рейтинг';
$_['error_captcha']            = 'Код підтвердження не збігається з зображенням!';

