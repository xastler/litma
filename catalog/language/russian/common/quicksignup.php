<?php
// Text
$_['text_signin_register']    = 'Sign In/Register';
$_['text_login']   			  = 'Sign In';
$_['text_register']    		  = 'Регистрация';

$_['text_new_customer']       = 'New Customer';
$_['text_returning']          = 'Авторизация';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'Email';
$_['entry_name']              = 'Name';
$_['entry_password']          = 'Пароль';
$_['entry_telephone']         = 'Telephone';
$_['text_forgotten']          = 'Забыли пароль?';
$_['text_agree']              = 'I agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['text_enter_social']              = 'Войти с помощью';



//Button
$_['button_login']            = 'Войти';

//Error
$_['error_name'] = 'Имя должно быть от 1 до 32 символов!';
$_['error_email'] = 'Адрес электронной почты не является действительным!';
$_['error_telephone'] = 'Телефон должен быть от 3 до 32 символов!';
$_['error_password'] = 'Пароль должен быть от 4 до 20 символов!';
$_['error_exists'] = 'Предупреждение: адрес электронной почты уже зарегистрирован!';
$_['error_agree'] = 'Предупреждение: вы должны согласиться c %s!';
$_['error_warning'] = 'Внимание! Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_approved'] = 'Предупреждение: ваша учетная запись требует подтверждения, прежде чем вы сможете войти в систему.';
$_['error_login'] = 'Предупреждение: нет соответствия для адреса электронной почты и / или пароля.';