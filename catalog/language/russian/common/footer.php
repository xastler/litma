<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Контакты';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнёры';
$_['text_special']      = 'SALE';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Мои Закладки';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_powered']      = '&copy; ООО “%s” 2015-%s. Все права защищены ';

$_['text_texxt']        = '„Мы будем рады с Вами сотрудничать, учитывая Ваши пожелания и коммерческий интерес“';
$_['text_name_seo']     = '<span>Михальський В.И.</span><br>Генеральный директор ООО “Литма”';
$_['text_emails']        = 'Отдел продаж:';
$_['text_download_prise']     = 'СКАЧАТЬ ПРАЙС-ЛИСТ';

$_['entry_name']      	= 'Имя';
$_['entry_phone']       = 'Номер мобильного телефона';
$_['text_call']      	= 'Заказать звонок';
$_['text_send']      	= 'Отправить';
$_['text_loading']      = 'Обработка';
$_['text_info_btn']     = 'Контакты';
$_['text_menu_btn']     = 'Каталог';