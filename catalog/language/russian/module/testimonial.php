<?php
// Text
$_['heading_title']	           = 'Отзывы';

$_['text_write']               = 'Оставить отзыв';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Благодарим вас за отзыв. Он был представлен веб-мастеру для утверждения.';

$_['text_mail_subject']        = 'You have a new testimonial (%s).';
$_['text_mail_waiting']	       = 'You have a new testimonial waiting.';
$_['text_mail_author']	       = 'Author: %s';
$_['text_mail_rating']	       = 'Rating: %s';
$_['text_mail_text']	       = 'Text:';

// Entry
$_['entry_name']               = 'Имя';
$_['entry_review']             = 'Сообщение';
$_['entry_email']              = 'Email';
$_['entry_rating']             = 'Рейтинг';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Button
$_['button_continue']          = 'Отправить';

// Error
$_['error_name']               = 'Имя отзыва должно быть от 3 до 25 символов!';
$_['error_text']               = 'Текст отзыва должен составлять от 25 до 3000 символов!';
$_['error_rating']             = 'Нужно выставить рейтинг!';
$_['error_captcha']            = 'Код подтверждения не соответствует изображению!';

