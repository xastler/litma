<?php if(isset($products)){ ?>
    <div class="watched_products">
        <h2><?php echo $heading_title; ?><?php //echo $text_visitedproduct; ?></h2>
        <?php
            $k = count($products);
            $class_array = array();
            if($k<=4){
                $class_array[] = 'hidden-lg';
            }
            if($k<=3){
                $class_array[] = 'hidden-md';
            }
            if($k<=2){
                $class_array[] = 'hidden-sm';
            }
            if($k==1){
                $class_array[] = 'hidden-xs';
            }
        ?>
        <div class="custom_slider_nav custom_slider_nav_dark flex">
            <span class="carousel_nav_prev <?php echo implode(' ',$class_array)?>"></span>
            <span class="carousel_nav_next <?php echo implode(' ',$class_array)?>"></span>
        </div>
        <div class="watched_products_carousel">
            <?php foreach ($products as $product) { ?>
                <?php $this->partial('product_item', array('product' => $product, 'button_cart' => $button_cart, 'button_details' => $button_details));?>
            <?php } ?>
        </div>
    </div>
<?php /*
<div>
<h3><?php echo $heading_title; ?></h3>
  <?php foreach ($products as $product) { ?>
  <div class="product-thumb">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" /></a>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
		
		<?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
		
		<?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
	</div>
  </div>
  <?php } ?>
</div>*/?>
<?php } ?>